#include "sdt_utils.h"

/*------------------------------------------------- 内部函数声明------------------------------------------------------*/
/* 将SID结构体转换为UINT8数组	*/
SDT_RC sdt_convert_sid_struct_to_array(UINT8* ptSIDArray, UINT32 SIDArraySize, SID_INFO_T const * ptSIDStruct, UINT32 smi);

/* CRC32算法 */ 
UINT32 tct_sdt_calculate_crc32(UINT8* array, UINT32 arraySize, UINT32 crcSeed);

/* 检测sdt参数存在性 */
SDT_RC sdt_check_sdt_config_parameter_exist(P_SDT_CONFIG_T pSdtConfig);

/* 检测参数合理性 */
SDT_RC sdt_check_sdt_config_parameter_valid(P_SDT_CONFIG_T pSdtConfig);



/*-------------------------------------------------- 接口定义-----------------------------------------------------*/

/*
*	函数名称:   sdt_calculate_sid
*	函数功能:   通过给定的SID信息结构体 和 给定的SMI 计算相应的SID
* 
*	参数说明:   SID_INFO_T*			pSIDStruct			SID信息结构体指针
*				UINT32				smi					SMI
*
*	返回结果:   非0		返回计算得到的SID值
*				0		计算SID值错误
*/
UINT32 sdt_calculate_sid(SID_INFO_T* pSIDStruct, UINT32 smi)
{
	UINT32	sidValue = 0;
	UINT32	sidArraySize = sizeof(SID_INFO_T) + sizeof(smi);
	UINT8	sidArray[sizeof(SID_INFO_T) + sizeof(smi)];

	/* 将SID结构体转换为UINT8数组	*/
	if (SDT_OK == sdt_convert_sid_struct_to_array(sidArray, sidArraySize, pSIDStruct, smi))
	{
		/* 通过CRC算法计算SID的值		*/
		sidValue = tct_sdt_calculate_crc32(sidArray, sidArraySize, SDT_SID_SEED);
	}

	return  sidValue;
}

/*
*	函数名称:   sdt_calculate_sc
*	函数功能:   通过给定的VDP数组，计算安全码
*
*	参数说明:   UINT8*			vdpArray			vdp数组
*				UINT32			vdpSCOffset			vdp数组中sc位置的偏移
*				UINT32			sid					种子值sid
*
*	返回结果:   非0				返回计算得到的sc值
*				0				计算sc值错误
*/
UINT32 sdt_calculate_sc(UINT8* vdpArray, UINT32 userDataLenth, UINT32 sid)
{
	UINT32 scValue = 0;
	UINT32 SCstructLenth=0;
	UINT32 vpdLenth=0;
	
	vpdLenth=sdsrc_four_byte_alignment(userDataLenth);
	SCstructLenth= vpdLenth+SDT_TAIL_SIZE- sizeof(UINT32);
	
	UINT8* ptempArray = (UINT8*)malloc(SCstructLenth);
	
	
	if (PT_IS_NULL(ptempArray))
	{
		scValue = 0u;
		
		printf("calculate safety code error!");
	}
	else
	{
		//set sc struct in tempArray
		memset((void*)ptempArray,0,SCstructLenth);
		
		memcpy((void*)ptempArray, (const void*)vdpArray, userDataLenth);
		memcpy((void*)(ptempArray+vpdLenth), (const void*)(vdpArray+userDataLenth), SDT_TAIL_SIZE - sizeof(UINT32));
		/* 计算SC */
		scValue = tct_sdt_calculate_crc32(ptempArray, SCstructLenth, sid);

	}

	/* 释放临时申请的数组 */
	free((void*)ptempArray);
	ptempArray = NULL;

	return scValue;
}

/*
*	函数名称:   sdt_get_rand_number
*	函数功能:   生成给定范围内([min, max])的随机数
*
*	参数说明:   UINT32			min					随机数下限
*				UINT32			max					随机数上限
*
*	返回结果:   UINT32			生成的随机数
* 
*/
UINT32 sdt_get_rand_number(UINT32 min, UINT32 max)
{
	srand((unsigned)time(NULL));
	return (UINT32)(rand()%(max-min+1))+min;
}


/*
*	函数名称:   sdt_check_config_valid
*	函数功能:   检测配置信息有效性
*	参数说明:   P_SDT_CONFIG_T			pSdtConfig				配置参数指针
*
*	返回结果:   SDT_OK					配置信息有效
*               SDT_PT_NOT_EXIST		配置信息指针不存在
*               SDT_INVALID_PARAMETER   相关配置参数错误
*
*/
SDT_RC sdt_check_config_valid(P_SDT_CONFIG_T pSdtConfig)
{
	SDT_RC result = SDT_OK;

	/* 检测传入的指针是否存在 */
	if (PT_IS_NULL(pSdtConfig))
	{
		result = SDT_PT_NOT_EXIST;
		return result;
	}

	/* 检测参数存在性 */
	result = sdt_check_sdt_config_parameter_exist(pSdtConfig);
	if (SDT_OK != result)
	{
		return result;
	}

	/* 检测参数合理性 */
	result = sdt_check_sdt_config_parameter_valid(pSdtConfig);
	if (SDT_OK != result)
	{
		return result;
	}

	return result;

}

/*
*	函数名称:	sdt_get_current_time
*	函数功能:	获取当前程序运行时间
*	参数说明:	无
*	返回结果:	SDT_CLOCK		当前程序运行时间
*/
SDT_CLOCK sdt_get_current_time()
{
#if ((defined(WIN32))|| (defined(WIN64)))
	return clock();
#elif(defined(VXWORKS))
	return (SDT_CLOCK)tickGet();
#endif
}



/*---------------------------------------------内部函数实现---------------------------------------------*/
/*
*	函数名称:   sdt_convert_sid_struct_to_array
*	函数功能:   将SID结构体转换为UINT8数组
*	参数说明:   UINT8 *				pSIDArray		返回SID数组的指针
*				UINT32				SIDArraySize	SID数组的大小
*				SID_INFO_T*			ptSIDStruct		SID结构体指针
*				UINT32				smi				smi
*
*	返回结果:   无
*/
SDT_RC sdt_convert_sid_struct_to_array(UINT8* pSIDArray, UINT32 SIDArraySize, SID_INFO_T const * ptSIDStruct, UINT32 smi)
{
	SDT_RC result = SDT_OK;
	UINT8* pos = pSIDArray;  
	if (NULL == pos)
	{
		SDT_DEBUG_MSG("[sdt_convert_sid_struct_to_array()]:sid info array pointer dose not exist!\n");
		result = SDT_PT_NOT_EXIST;
		return result;
	}
	
	/* 清空数组 */
	memset((void*)pSIDArray, 0, SIDArraySize);

	/* 写入smi */
	LongToChar(smi, pos);
	pos += sizeof(smi);

	/* 写入保留值01 */
	ShortToChar(ptSIDStruct->reserved01, pos);
	pos += sizeof(ptSIDStruct->reserved01);

	/* 写入SDT版本号 */
	ShortToChar(ptSIDStruct->SDTProtVers, pos);
	pos += sizeof(ptSIDStruct->SDTProtVers);

	/* 写入UUID */
	memcpy((void*)pos, ptSIDStruct->cstUUID, SDT_UUID_SIZE);
	pos += SDT_UUID_SIZE;

	/* 写入拓扑操作计数 */
	LongToChar(ptSIDStruct->safeTopoCount, pos);
	pos += sizeof(ptSIDStruct->safeTopoCount);

	/* 写入保留值02 */
	LongToChar(ptSIDStruct->reserved02, pos);
	pos += sizeof(ptSIDStruct->reserved02);

	/* 越界检查 */
	if (pos != (pSIDArray + SIDArraySize))
	{
		SDT_DEBUG_MSG("[sdt_convert_sid_struct_to_array()]:Out of bounds occurred during array conversion!\n");
		result = SDT_OUT_OF_BOUND;
	}

	return result;
}

/*
*	函数名称:   sdt_calculate_sc
*	函数功能:   CRC32算法
*
*	参数说明:   UINT8*			array				数组
*				UINT32			arraySize			数组长度
*				UINT32			crcSeed				种子值
*
*	返回结果:   非0				返回计算得到的值
*				0				计算值错误
*/
UINT32 tct_sdt_calculate_crc32(UINT8* array, UINT32 arraySize, UINT32 crcSeed)
{
	UINT32 i; /* 计数 */
	UINT32 crcValue = crcSeed;
	for (i = 0; i < arraySize; ++i)
	{
		crcValue = g_tct_sdt_crc32_table[((UINT32)(crcValue >> 24) ^ *(array + i)) & 0xff] ^ (crcValue << 8);
	}
	return crcValue;
}

/*
*	函数名称:   sdt_check_sdt_config_parameter_exist
*	函数功能:   检测sdt参数存在性
*	参数说明:   P_SDT_CONFIG_T			pSdtConfig			sdt配置参数指针
*
*	返回结果:   SDT_OK					 相关配置参数正确
*               SDT_INVALID_PARAMETER    相关配置参数错误
*/
SDT_RC sdt_check_sdt_config_parameter_exist(P_SDT_CONFIG_T pSdtConfig)
{
	SDT_RC result = SDT_OK;

	/* 必要参数存在性检测 */
	/* smi1 检测 */
	if (SDT_INVALID_VALUE == pSdtConfig->smi1)
	{
		fputs("parameter: \"smi1\" does not exist!\n", stderr);
		result |= SDT_INVALID_PARAMETER;
	}

	/* udv 检测*/
	if (SDT_INVALID_VALUE == pSdtConfig->udv)
	{
		fputs("parameter: \"udv\" does not exist!\n", stderr);
		result |= SDT_INVALID_PARAMETER;
	}

	/* rxPeriod 检测 */
	if (SDT_INVALID_VALUE == pSdtConfig->rxPeriod)
	{
		fputs("parameter: \"rxPeriod\" does not exist!\n", stderr);
		result |= SDT_INVALID_PARAMETER;
	}

	/* txPeriod 检测 */
	if (SDT_INVALID_VALUE == pSdtConfig->txPeriod)
	{
		fputs("parameter: \"txPeriod\" does not exist!\n", stderr);
		result |= SDT_INVALID_PARAMETER;
	}

	/* 可选参数存在性检测, 不存在, 则赋值为默认值 */
	if (SDT_INIT_VALUE == pSdtConfig->smi2)		pSdtConfig->smi2 =	SDT_DEFAULT_CHANNEL_B_SMI;
	if (SDT_INIT_VALUE == pSdtConfig->nRxSafe)	pSdtConfig->nRxSafe = SDT_DEFAULT_N_RXSAFE;
	if (SDT_INIT_VALUE == pSdtConfig->nGuard)	pSdtConfig->nGuard = SDT_DEFAULT_N_GUARD;
	if (SDT_INIT_VALUE == pSdtConfig->cmThr)	pSdtConfig->cmThr = SDT_DEFAULT_CM_THR;
	if (SDT_INIT_VALUE == pSdtConfig->channelIdentity) pSdtConfig->channelIdentity = STA_SDSINK_CHANNEL;

	return result;
}

/*
*	函数名称:   sdt_check_sdt_config_parameter_valid
*	函数功能:   检测sdt参数合理性
*	参数说明:   P_SDT_CONFIG_T				pSdtConfig			sdt配置参数指针
*
*	返回结果:   SDT_OK						相关配置参数正确
*               SDT_INVALID_PARAMETER		相关配置参数错误
*/
SDT_RC sdt_check_sdt_config_parameter_valid(P_SDT_CONFIG_T pSdtConfig)
{
	SDT_RC result = SDT_OK;

	/* smi1 和 smi2 合理性检测 */
	/* smi1 和 smi2 不能同时为0, 存在性检测已经覆盖 */
	/* smi1 和 smi2 应当互斥 */
	if (pSdtConfig->smi1 == pSdtConfig->smi2)
	{
		fprintf(stderr,"unreasonable parameter: \"smi1\" and \"pSdtConfig->smi2\" have the same value!\n");
		result |= SDT_INVALID_PARAMETER;
	}
	/* TODO 是否需要添加 smi的值在1-999范围内的判断 */
	if ((1 <= pSdtConfig->smi1) && (999 >= pSdtConfig->smi1))
	{
		fprintf(stderr, "Warning: smi1 is %u, in the range [1-999]\n", pSdtConfig->smi1);
	}
	if ((1 <= pSdtConfig->smi2) && (999 >= pSdtConfig->smi2))
	{
		fprintf(stderr, "Warning: smi2 is %u, in the range [1-999]\n", pSdtConfig->smi2);
	}


	/* udv 合理性检测 */
	if ((SDT_UDV_MIN > pSdtConfig->udv) || (SDT_UDV_MAX < pSdtConfig->udv))
	{
		fprintf(stderr, "unreasonable parameter: \"udv\" should be in the range %u - %u\n", SDT_UDV_MIN, SDT_UDV_MAX);
		result |= SDT_INVALID_PARAMETER;
	}

	/* 时间参数 合理性检测 */
	if ((SDT_PERIOD_MIN > pSdtConfig->rxPeriod) || (SDT_PERIOD_MAX < pSdtConfig->rxPeriod))
	{
		fprintf(stderr, "unreasonable parameter: \"rxPeriod\" should be in the range %u - %u\n", SDT_PERIOD_MIN, SDT_PERIOD_MAX);
		result |= SDT_INVALID_PARAMETER;
	}
	if ((SDT_PERIOD_MIN > pSdtConfig->txPeriod) || (SDT_PERIOD_MAX < pSdtConfig->txPeriod))
	{
		fprintf(stderr, "unreasonable parameter: \"txPeriod\" should be in the range %u - %u\n", SDT_PERIOD_MIN, SDT_PERIOD_MAX);
		result |= SDT_INVALID_PARAMETER;
	}
	if (pSdtConfig->rxPeriod > pSdtConfig->txPeriod)
	{
		fprintf(stderr, "unreasonable parameter: \"txPeriod\" should be bigger than \"rxPeriod\", \"txPeriod\": %u , \"rxPeriod\": %u\n", pSdtConfig->txPeriod, pSdtConfig->rxPeriod);
		result |= SDT_INVALID_PARAMETER;
	}

	/* nRxSafe 合理性检测 */
	if ((SDT_N_SAFE_MIN > pSdtConfig->nRxSafe) || (SDT_N_SAFE_MAX < pSdtConfig->nRxSafe))
	{
		fprintf(stderr, "unreasonable parameter: \"nRxSafe\" should be in the range %u - %u\n", SDT_N_SAFE_MIN, SDT_N_SAFE_MAX);
		result |= SDT_INVALID_PARAMETER;
	}

	/* nGuard 合理性检测*/
	if ((SDT_N_GUARD_MIN > pSdtConfig->nGuard) || (SDT_N_GUARD_MAX < pSdtConfig->nGuard))
	{
		fprintf(stderr, "unreasonable parameter: \"nGuard\" should be in the range %u - %u\n", SDT_N_GUARD_MIN, SDT_N_GUARD_MAX);
		result |= SDT_INVALID_PARAMETER;
	}

	/* 通道监视阈值 合理性检测*/
	if (SDT_DEFAULT_CM_THR != pSdtConfig->cmThr)
	{
		fprintf(stderr, "Recommend %u, it is not recommended to modify at this time\n", SDT_DEFAULT_CM_THR);
		result |= SDT_INVALID_PARAMETER;
	}

	/* 通道身份 合理性检测 */
	if ((STA_SDSINK_CHANNEL != pSdtConfig->channelIdentity) &&
		(STA_SDSRC_CHANNEL_A != pSdtConfig->channelIdentity) &&
		(STA_SDSRC_CHANNEL_B != pSdtConfig->channelIdentity))
	{
		fprintf(stderr, "unreasonable parameter: \"channelIdentity\" channel identify should be: [STA_SDSINK_CHANNEL|STA_SDSRC_CHANNEL_A|STA_SDSRC_CHANNEL_B]\n");
		result |= SDT_INVALID_PARAMETER;
	}
	/* 检测通道和传入的smi值是否匹配 */
	/* 通道身份为 发送端通道B时 需要保证smi2存在*/
	if ((STA_SDSRC_CHANNEL_B == pSdtConfig->channelIdentity) && (SDT_INIT_VALUE == pSdtConfig->smi2))
	{
		fprintf(stderr, "unreasonable parameter: \"channelIdentity\" , smi2 does not exist!\n");
		result |= SDT_INVALID_PARAMETER;
	}

	return result;

}

/* 返回4的整倍数 */ 
UINT32 sdsrc_four_byte_alignment(UINT32 length)
{
	return length + (4u - (length % 4u)) % 4u;
}

/*
*  功能描述：  用于将2字节数据变为UINT16， BIG endian
*  参数说明：  pInput, 为输入
*  返回值：    变换后的UINT16值
*/
UINT16 ShortFromChar(const UINT8 *pInput)
{
    UINT16 Tempshort;
    Tempshort = ( *(pInput) );
    Tempshort = (UINT16)( Tempshort<<8 ) + *(pInput+1) ;  
    return Tempshort;
}

/*
*  功能描述：  用于将4字节数据变为UINT32， BIG endian
*  参数说明：  pInput, 为输入
*  返回值：    变换后的UINT32值
*/
UINT32 LongFromChar(const UINT8 *pInput)
{
    UINT32 Templong;
    Templong = ( *(pInput) );
    Templong = (UINT32)( Templong<<8 ) +  *(pInput+1) ;
    Templong = (UINT32)( Templong<<8 ) +  *(pInput+2) ;
    Templong = (UINT32)( Templong<<8 ) +  *(pInput+3) ;
    return Templong;
}

/*
*  功能描述：  将2个字节长的整型变为字节表示  BIG endian
*  参数说明：  Input, 为输入
*              pOutput,为输出
*  返回值：  
*/
void ShortToChar ( UINT16 Input, UINT8 *pOutput)
{
    *pOutput =  (Input>>8) & 0xff;
    *(pOutput + 1) = Input & 0xff;
}

/*
*  功能描述：  将4个字节长的整型变为字节表示  BIG endian
*  参数说明：  Input, 为输入
*              pOutput,为输出
*  返回值：  
*/
void LongToChar(UINT32 Input, UINT8 *pOutput)
{
    *pOutput =  (UINT8)((Input>>24) & 0xff);
    *(pOutput + 1) =  (UINT8)((Input>>16) & 0xff);
    *(pOutput + 2) =  (UINT8)((Input>>8) & 0xff);
    *(pOutput + 3) = (UINT8)(Input & 0xff);
}
