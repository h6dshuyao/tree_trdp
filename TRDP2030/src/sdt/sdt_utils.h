#ifndef SDT_UTILS_H
#define SDT_UTILS_H

#include <stdio.h>
#include <stdlib.h>

#if ((defined(WIN32))|| (defined(WIN64)))
#include <time.h>
#endif

#include "sdt_type.h"

/*-----------------------------------------------宏定义--------------------------------------------*/

/*
*	函数名称:   sdt_calculate_sid
*	函数功能:   通过给定的SID信息结构体 和 给定的SMI 计算相应的SID
* 
*	参数说明:   SID_INFO_T*		pSIDStruct			SID信息结构体指针
*				UINT32			smi					SMI
*
*	返回结果:   非0				返回计算得到的SID值
*				0				计算SID值错误
*/
UINT32 sdt_calculate_sid(SID_INFO_T* pSIDStruct, UINT32 smi);

/*
*	函数名称:   sdt_calculate_sc
*	函数功能:   通过给定的VDP数组，计算安全码
* 
*	参数说明:   UINT8*			vdpArray			vdp数组		
*				UINT32			vdpSCOffset			vdp数组中sc位置的偏移
*				UINT32			sid					种子值sid
*
*	返回结果:   非0				返回计算得到的sc值
*				0				计算sc值错误
*/

UINT32 sdt_calculate_sc(UINT8* vdpArray, UINT32 userDataLenth, UINT32 sid);

/*
*	函数名称:   sdt_get_rand_number
*	函数功能:   生成给定范围内([min, max])的随机数
*
*	参数说明:   UINT32			min					随机数下限
*				UINT32			max					随机数上限
*
*	返回结果:   UINT32			生成的随机数
* 
*/
UINT32 sdt_get_rand_number(UINT32 min, UINT32 max);

/*
*	函数名称:   sdt_check_config_valid
*	函数功能:   检测配置信息有效性
*	参数说明:   P_SDT_CONFIG_T		pSdtConfig     配置参数指针
*
*	返回结果:   SDT_OK              配置信息有效
*               SDT_PT_NOT_EXIST    配置信息无效
*/
SDT_RC sdt_check_config_valid(P_SDT_CONFIG_T pSdtConfig);

/*
*	函数名称:	sdt_get_current_time
*	函数功能:	获取当前程序运行时间
*	参数说明:	无
*	返回结果:	SDT_CLOCK		当前程序运行时间
*/
SDT_CLOCK sdt_get_current_time();
UINT32 sdsrc_four_byte_alignment(UINT32 length);

void ShortToChar ( UINT16 Input, UINT8 *pOutput);
void LongToChar(UINT32 Input, UINT8 *pOutput);
UINT32 LongFromChar(const UINT8 *pInput);
UINT16 ShortFromChar(const UINT8 *pInput);

#endif
