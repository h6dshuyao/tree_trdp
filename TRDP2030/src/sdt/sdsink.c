#include "sdsink.h"
#include "sdt_utils.h"
#include <vos_thread.h>
#include "vos_utils.h"

/*---------------------------------------内部变量定义-------------------------------------------------*/

SDSINK	g_sdsink = { .channelCapacity = SDSINK_CHANNEL_AMOUNT_MAX };	/* 保存当前接收端的全部通道信息 */
VOS_MUTEX_T pmyPrintMutex;	//接收信息打印锁

/*---------------------------------------内部函数声明--------------------------------------------------*/


SDT_RC sdsink_config_sdsink_channel(SDSINK_CHANNEL* pSdsinkChannel, SDT_CONFIG_T const* pSdsinkConfig);	/* 配置发送端通道 */	

UINT32 sdsink_get_last_VDP_SSC(SDSINK_CHANNEL* pSdsinkChannel);	/* 获取上一VDP的SSC */ 
UINT32 sdsink_get_current_sid(SDSINK_CHANNEL* pSdsinkChannel);	/* 获取当前的SID */

SDT_RC sdsink_prepare_parsing(SDSINK_CHANNEL* pSdsinkChannel, UINT8 * vdpBuffer, UINT32 vpdLength);	/* 解析vdp信息 为校验做准备*/ 
void sdsink_update_parsing_environment(SDSINK_CHANNEL* pSdsinkChannel);	/* 准备解析环境 为解析做准备 */
SDT_RC sdsink_get_vdp_info(VDP_INFO_T* vdpInfoStruct, UINT8 * vdpBuffer, UINT32 vdpTailOffset);	/* 提取VDP信息 */ 

void sdsink_check_vdp_type(SDSINK_CHANNEL * pSdsinkChannel);				/* 判断VDP类型 */
void sdsink_check_vdp_is_correct(SDSINK_CHANNEL * pSdsinkChannel);			/* 判断 VDP 是否为正确的VDP */
void sdsink_check_vdp_is_repeated(SDSINK_CHANNEL* pSdsinkChannel);			/* 判断 VDP 是否为重复的VDP */
void sdsink_check_vdp_is_initial(SDSINK_CHANNEL* pSdsinkChannel);			/* 判断 VDP 是否为初始的VDP */
void sdsink_check_vdp_is_new(SDSINK_CHANNEL * pSdsinkChannel);				/* 判断 VDP 是否为新的VDP */
void sdsink_check_vdp_is_valid(SDSINK_CHANNEL * pSdsinkChannel);			/* 判断 VDP 是否为有效的VDP */

void sdsink_check_communication_safe_state(SDSINK_CHANNEL* pSdsinkChannel);	/* SDT通信状态判断 */
void sdsink_check_rts_communication_safe(SDSINK_CHANNEL* pSdsinkChannel);	/* 宿时间监视的通信安全判断 */ 
void sdsink_check_gtc_communication_safe(SDSINK_CHANNEL*  pSdsinkChannel);	/* 守护时间监视的通信安全判断 */
void sdsink_check_lmc_communication_safe(SDSINK_CHANNEL* pSdsinkChannel);	/* 延迟时间监视的通信安全判断 */
void sdsink_check_cmc_communication_safe(SDSINK_CHANNEL* pSdsinkChannel);	/* 通道监视的通信安全判断 */ 


void sdsink_save_test_info(SDT_HANDLE sdsinkHandle);/* 打印一行测试信息*/
void print_counters(SDT_HANDLE sdsinkHandle);/* 打印当前一个通道的统计信息*/



/* 20210820 正式后 添加新函数 */
/* 重置sdsink 上电/重启后调用该函数, 宿时间监视发生不安全后调用该函数 */
void sdsink_reset_vdp_parsing_environment(SDSINK_CHANNEL * pSdsinkChannel);

/* 更新统计信息 */
UINT32 sdsink_update_statistic_info(SDSINK_CHANNEL* pSdsinkChannel, SDT_HANDLE sdsinkHandle);

/* 重置统计信息 */
void sdsink_reset_statistic_info(SDSINK_STATISTIC_INFO_T* pStatInfo);


/*---------------------------------------公共接口实现---------------------------------------------------*/
/*
*	函数名称:	sdsink_create_channel
*	函数功能:	创建 并 配置 接收端通道
*
*	参数说明:	SDT_HANDLE*			pSdsinkHandle	发送端句柄指针
*				SDT_CONFIG_T*		pSdsinkConfig	发送端通道配置信息
* 
*	返 回 值:	SDT_OK					创建成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_PARAMETER   相关配置参数错误
*				SDSINK_CHANNEL_FULL		当前使用通道到达上限
*				SDT_INVALID_SID			计算SID错误
* 
*/
void mutexinit()
{
	VOS_ERR_T err;
	err=vos_mutexCreate(&pmyPrintMutex);
}
/*
*	函数名称:	sdsink_create_channel
*	函数功能:	创建 并 配置 接收端通道
*
*	参数说明:	SDT_HANDLE*			pSdsinkHandle	发送端句柄指针
*				SDT_CONFIG_T*		pSdsinkConfig	发送端通道配置信息
* 
*	返 回 值:	SDT_OK					创建成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_PARAMETER   相关配置参数错误
*				SDSINK_CHANNEL_FULL		当前使用通道到达上限
*				SDT_INVALID_SID			计算SID错误
* 
*/


SDT_RC sdsink_create_channel(SDT_HANDLE* pSdsinkHandle, SDT_CONFIG_T * pSdsinkConfig)
{
	
	SDT_RC		result = SDT_OK;
	SDT_HANDLE	tempHandle = g_sdsink.channelNum;	/* 获取接收端第一个未被占用的下标 */
	P_SDSINK_CHANNEL tempSdsinkChannel = NULL;

	/* 判断传入的句柄指针和配置参数指针是否存在 */
	if ((PT_IS_NULL(pSdsinkHandle)) || (PT_IS_NULL(pSdsinkConfig)))
	{
		SDT_DEBUG_MSG("sdsink channel create failed, [ pSdsinkHandle || pSdsinkConfig ] pointer does not exist!\n");
		result = SDT_PT_NOT_EXIST;
		return result;
	}

	/* 判断传入的接收端参数是否合理 */
	result = sdt_check_config_valid(pSdsinkConfig);
	if (SDT_OK != result)
	{
		SDT_DEBUG_MSG("sdsink channel create error, configuation parameters are invalide!\n");
		return result;
	}

	/* 申请通道 */
	if (tempHandle >= g_sdsink.channelCapacity)
	{
		/* 当前使用通道数量达到上限 */
		SDT_DEBUG_MSG("sdsink channel create error, channel array is full!\n");
		result = SDSINK_CHANNEL_FULL;
		return result;
	}

	/* 获取接收端通道 */
	tempSdsinkChannel = &(g_sdsink.channelArray[tempHandle]);
	if (PT_IS_NULL(tempSdsinkChannel))
	{
		SDT_DEBUG_MSG("sdsink create failed, can not get channel pointer!\n");
		result = SDT_PT_NOT_EXIST;		/* 未获取到发送端通道指针 */
		return result;
	}
	else
	{
		result = sdsink_config_sdsink_channel(tempSdsinkChannel, pSdsinkConfig);
	}

	if (SDT_OK != result)
	{
		memset((void*)tempSdsinkChannel, 0, sizeof(SDSINK_CHANNEL));
		SDT_DEBUG_MSG("SDSINK : channel %u create failed! \n", tempHandle);
	}
	else
	{
		
		g_sdsink.channelNum++;			/* 配置完成,通道数量增加	*/
		*pSdsinkHandle = tempHandle;	/* 返回句柄值				*/
		SDT_DEBUG_MSG("SDSINK : channel %u create success! \n", tempHandle);
	}

	return result;
}

/*
*   函数名称:   sdsink_parse_VDP
*   函数功能:   根据句柄对应的通道, 解析收到的PD数据
* 
*   输入参数:   SDT_HANDLE		sdsinkHandle            接收端句柄
*				UINT8*			vdpBuffer				vdp数据
*				UINT16			vdpLength				vdp数据中vpd的长度
*				UINT8*			vpdBuffer				解析后的vpd数据
*				UINT32*			pVpdLength				解析后数据的长度
*
*   返 回 值:   SDT_OK					解析成功
*				SDT_INVALID_HANDLE		无效句柄
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_VPD_LEN		VPD长度不合理
*				SDT_INVALID_SC			计算SC错误
*				SDT_OUT_OF_BOUND		提取信息过程中访问越界
* 
*/
SDT_RC sdsink_parse_VDP(SDT_HANDLE sdsinkHandle, UINT8 * vdpBuffer, UINT32 vdpLength, UINT8 * vpdBuffer, UINT32* pVpdLength)
{

	SDT_RC	result = SDT_OK;			
	UINT32	tempVpdLength = 0;
	P_SDSINK_CHANNEL tempSdsinkChannel = NULL;

	/* 判断 传入的接收端句柄是否合理	*/ 
	if (SDSINK_HANDLE_IS_INVALIDE(sdsinkHandle))
	{
		SDT_DEBUG_MSG("invalid sdsink handle!\n");
		result = SDT_INVALID_HANDLE;
		return result;
	}

	/* 检测传入的指针是否存在 */
	if (PT_IS_NULL(vdpBuffer) || PT_IS_NULL(vpdBuffer) || PT_IS_NULL(pVpdLength))
	{
		SDT_DEBUG_MSG("[vdpBuffer || vpdBuffer || pVpdLength] pointer does not exist!\n");
		result = SDT_PT_NOT_EXIST;
		return result;
	}

	/* 判断传入的vpd长度是否合理 */
	if (((SDT_VPDMIN + SDT_TAIL_SIZE) > vdpLength) || ((SDT_VPDMAX + SDT_TAIL_SIZE) < vdpLength))
	{
		SDT_DEBUG_MSG("vdp length invalide! should be [16-1000], current is %u\n", vdpLength);
		result = SDT_INVALID_VPD_LEN; 
		return result;
	}

	tempVpdLength = vdpLength - SDT_TAIL_SIZE;
	tempSdsinkChannel = &(g_sdsink.channelArray[sdsinkHandle]);

	/* 准备 解析环境	*/ 
	result = sdsink_prepare_parsing_environment(tempSdsinkChannel, vdpBuffer, tempVpdLength);

	/* VDP类型判断		*/ 
	sdsink_check_vdp_type(tempSdsinkChannel);


	/* SDT通信状态判断	*/ 
	sdsink_check_communication_safe_state(tempSdsinkChannel);

	/* 更新统计信息    */
	UINT32 telpackcounter;
	telpackcounter=sdsink_update_statistic_info(tempSdsinkChannel, sdsinkHandle);

	if (5000==telpackcounter)
	{
		print_counters(sdsinkHandle);
		sdsink_reset_statistic_info(&(tempSdsinkChannel->m_statisticInfo));
	}

	/* 判断通信数据是否有效	*/ 
	if ((SDT_SAFE == tempSdsinkChannel->m_safeComFlag) && (SDT_TRUE == tempSdsinkChannel->m_validVDPFlag))
	{
		/* 通道安全且 数据有效, 将接收到的VPD信息返回 */ 
		*pVpdLength = tempVpdLength;
		memcpy((void*)vpdBuffer, (const void*)vdpBuffer, (size_t)tempVpdLength);
	}
	else
	{
		/* 否则返回空数据包 */  
		*pVpdLength = SDT_INVALID_VALUE;
		memset((void*)vpdBuffer, SDT_INVALID_VALUE, tempVpdLength);
		result = SDT_INVALID_VPD;
	}

   // struct timespec tpStartTick;
   // struct timespec tpEndTick;
    //clock_gettime(CLOCK_REALTIME, &tpStartTick);

	//if(sdsinkHandle<5)
	//sdsink_save_test_info(sdsinkHandle);
	//printf("sdsink_save_test_info is okay\n");
   // clock_gettime(CLOCK_REALTIME, &tpEndTick);
   // long long costTimeMs = (tpEndTick.tv_sec-tpStartTick.tv_sec)*1000 + (tpEndTick.tv_nsec-tpStartTick.tv_nsec)/1000000;

   // printf("[%d]print cost time %lld ms \n",sdsinkHandle,costTimeMs);
	
	return result;
}


/*------------------------------------- 内部函数实现--------------------------------------------------*/
/* 配置发送端通道 */
SDT_RC sdsink_config_sdsink_channel(SDSINK_CHANNEL* pSdsinkChannel, SDT_CONFIG_T const* pSdsinkConfig)
{
	SDT_RC result = SDT_OK;

	/*	将通道状态标志位 置为激活	*/
	pSdsinkChannel->m_handleEffective = SDT_EFFECTIVE;

	/*  时间参数设置		*/
	pSdsinkChannel->m_txPeriod	= pSdsinkConfig->txPeriod;
	pSdsinkChannel->m_rxPeriod	= pSdsinkConfig->rxPeriod;
	pSdsinkChannel->m_rxSafe	= pSdsinkConfig->txPeriod * pSdsinkConfig->nRxSafe;
	pSdsinkChannel->m_tGuard	= pSdsinkConfig->txPeriod * pSdsinkConfig->nGuard;

	/*  通道监控阈值设置	*/
	pSdsinkChannel->m_cmThr = pSdsinkConfig->cmThr;

	/*  预期用户数据版本	*/
	pSdsinkChannel->m_udv = pSdsinkConfig->udv;

	/*  smi设置				*/
	pSdsinkChannel->m_channelASMI = pSdsinkConfig->smi1;
	pSdsinkChannel->m_channelBSMI = pSdsinkConfig->smi2;
	if (SDT_DEFAULT_CHANNEL_B_SMI == pSdsinkChannel->m_channelBSMI)
	{
		pSdsinkChannel->m_redundancyOpenFlag = SDT_FALSE;
	}
	else
	{
		pSdsinkChannel->m_redundancyOpenFlag = SDT_TRUE;
	}

	/*  设置SID结构体参数	*/
	memset((void*)&(pSdsinkChannel->m_SIDInfo), 0, sizeof(SID_INFO_T));
	pSdsinkChannel->m_SIDInfo.SDTProtVers = SDT_VERSION;

	/*  计算SID				*/
	pSdsinkChannel->m_channelASID = sdt_calculate_sid(&(pSdsinkChannel->m_SIDInfo), pSdsinkChannel->m_channelASMI);
	if (0 == pSdsinkChannel->m_channelASID)
	{
		result = SDT_INVALID_SID;
		return result;
	}
	if (SDT_TRUE == pSdsinkChannel->m_redundancyOpenFlag)
	{
		pSdsinkChannel->m_channelBSID = sdt_calculate_sid(&(pSdsinkChannel->m_SIDInfo), pSdsinkChannel->m_channelBSMI);
		if (0 == pSdsinkChannel->m_channelBSID)
		{
			result = SDT_INVALID_SID;
			return result;
		}
	}
	else
	{
		pSdsinkChannel->m_channelBSID = SDT_INVALID_VALUE;
	}

	/*	计算期望接收SSC的窗口大小, 向上取整	*/
	pSdsinkChannel->m_recvWindowSize = (UINT32)((pSdsinkChannel->m_rxSafe + pSdsinkChannel->m_txPeriod - 1) / pSdsinkChannel->m_txPeriod);

	/*	通道监视 VDP每小时发送频率			*/
	pSdsinkChannel->m_sendFrequencyPerHour	= (UINT32)(3600000 / pSdsinkChannel->m_txPeriod);			/* 3600000ms/tx_period */ 
	pSdsinkChannel->m_wrongIncreaseStep		= (UINT32)(pSdsinkChannel->m_sendFrequencyPerHour / K4);
	pSdsinkChannel->m_wrongCounterUpper		= (UINT32)((pSdsinkChannel->m_sendFrequencyPerHour / K4) * K3);

	pSdsinkChannel->m_currentProcessStartTime = SDT_INIT_VALUE;

	/*  重置vdp解析环境 */
	sdsink_reset_vdp_parsing_environment(pSdsinkChannel);

	/*  重置统计信息    */
	sdsink_reset_statistic_info(&(pSdsinkChannel->m_statisticInfo));

	return result;
}

/*
*	函数名称:	sdsink_prepare_parsing
*	函数功能:	准备解析环境, 配置解析环境的相关变量, 提取VDP中的相关信息
*	参数说明:	SDT_HANDLE			sdsinkHandle			接收端通道句柄
*				UINT8*				vdpBuffer				VDP数据缓存
*				UINT16				vpdLength				VPD数据长度
*
*	返 回 值:	SDT_OK				解析环境准备成功
*				SDT_PT_NOT_EXIST	指针不存在
*				SDT_OUT_OF_BOUND	提取信息过程中访问越界
*				SDT_INVALID_SC		计算SC错误
*/
SDT_RC sdsink_prepare_parsing(SDSINK_CHANNEL* pSdsinkChannel, UINT8 * vdpBuffer, UINT32 vpdLength)
{
	SDT_RC result = SDT_OK;
	UINT32 vdpSCOffset = 0;

	/* 准备解析环境，包括更新和初始化通道中的变量等	*/ 
	sdsink_update_parsing_environment(pSdsinkChannel);

	/* 提取VDP信息      */
	result = sdsink_get_vdp_info(&(pSdsinkChannel->m_VDPInfo), vdpBuffer, vpdLength);
	if (SDT_OK != result)
	{
		SDT_DEBUG_MSG("get vdp information failed!\n");
		return result;
	}
	
	/* 计算 通道A的SC并更新通道 */ 
	vdpSCOffset = (vpdLength + sizeof(VDP_INFO_T)) - sizeof(UINT32);
	/*printf("vdpBuffer:");
	for(int q=0;q<vpdLength+16;q++)
		printf("%03d",vdpBuffer[q]);*/
	//printf("\nSSC:%d SC:%d udv:%d r1:%d r2:%d\n",pSdsinkChannel->m_VDPInfo.m_SafeSequCount,pSdsinkChannel->m_VDPInfo.m_SafetyCode,pSdsinkChannel->m_VDPInfo.m_UserDataVersion,pSdsinkChannel->m_VDPInfo.m_reserved01,pSdsinkChannel->m_VDPInfo.m_reserved02);
	
	pSdsinkChannel->m_channelASC = sdt_calculate_sc(vdpBuffer, vpdLength, pSdsinkChannel->m_channelASID);
	if (0 == pSdsinkChannel->m_channelASC)
	{
		SDT_DEBUG_MSG("channel A : safety code calculate failed!\n");
		result = SDT_INVALID_SC;
		return result;
	}
	//printf("sc we save:%d sc we calculate:%d\n",pSdsinkChannel->m_VDPInfo.m_SafetyCode,pSdsinkChannel->m_channelASC);
	
	/* 计算 通道B的SC */
	if (SDT_TRUE == pSdsinkChannel->m_redundancyOpenFlag)
	{
		pSdsinkChannel->m_channelBSC = sdt_calculate_sc(vdpBuffer, vpdLength, pSdsinkChannel->m_channelBSID);
		if (0 == pSdsinkChannel->m_channelBSC)
		{
			SDT_DEBUG_MSG("channel B : safety code calculate failed!\n");
			result = SDT_INVALID_SC;
			return result;
		}
	}
	else
	{
		pSdsinkChannel->m_channelBSC = SDT_INVALID_VALUE;
	}

	return result;
}


/*
*	函数名称:	sdsink_prepare_parsing_environment
*	函数功能:	初始化解析环境
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_update_parsing_environment(SDSINK_CHANNEL* pSdsinkChannel)
{
	/* 程序运行计时器应当每周期更新 */
	pSdsinkChannel->m_currentProcessStartTime = sdt_get_current_time();

	/* 如果发生宿时间丢失 */
	if ((SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom) || (SDT_TRUE == pSdsinkChannel->m_powerOnResetFlag))
	{
		//printf("-------------reset sdsink 被触发--------------------\n");
		sdsink_reset_vdp_parsing_environment(pSdsinkChannel);
	}
	else
	{
		/* 上一VDP是初始VDP时, 保存以下变量 */
		if (SDT_TRUE == pSdsinkChannel->m_initialVDPFlag)
		{
			pSdsinkChannel->m_initialVDPSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;	/* 保存 初始SSC */
			pSdsinkChannel->m_lastValidVDPSSC = SDT_INIT_VALUE;								/* 初始化 上一有效VDPSSC */
																
			/* 保存 初始SID */
			if (STA_SDSRC_CHANNEL_A == pSdsinkChannel->m_sdsrcIdentity)
			{
				pSdsinkChannel->m_initialSID = pSdsinkChannel->m_channelASID;
			}
			else if (STA_SDSRC_CHANNEL_B == pSdsinkChannel->m_sdsrcIdentity)
			{
				pSdsinkChannel->m_initialSID = pSdsinkChannel->m_channelBSID;
			}
			/* 保存上一VDP接收端通道识别身份	*/
			pSdsinkChannel->m_lastSdsrcIdentity = pSdsinkChannel->m_sdsrcIdentity;
		}
		else if(SDT_TRUE == pSdsinkChannel->m_validVDPFlag)
		{
			/* 上一VDP是有效VDP时, 保存以下变量 */
			
			pSdsinkChannel->m_lastVDPIsNewFlag	= pSdsinkChannel->m_newVDPFlag;				/* 保存上一VDP的新VDP的标志位		*/
			pSdsinkChannel->m_lastSdsrcIdentity = pSdsinkChannel->m_sdsrcIdentity;			/* 保存上一VDP接收端通道识别身份	*/
			pSdsinkChannel->m_lastValidVDPSSC	= pSdsinkChannel->m_VDPInfo.m_SafeSequCount;/* 保存上一VDP的SSC */
		}

		/* vdp解析数据清零 但不处理通道身份，这使得如果收到错误VDP，通道身份将保持在最近的正确vdp的通道身份 */
		memset((void*)&(pSdsinkChannel->m_VDPInfo), 0, sizeof(VDP_INFO_T));

		pSdsinkChannel->m_channelASC = SDT_INIT_VALUE;
		pSdsinkChannel->m_channelBSC = SDT_INIT_VALUE;

	}
}

/*
*	函数名称:	sdsink_get_vdp_info
*	函数功能:	提取VDP信息
*	参数说明:	VDP_INFO_T*			vdpInfoStruct		vdp信息结构体
*				UINT8*				vdpBuffer			VDP信息缓存
*				UINT32				vdpTailOffset		VDP尾偏移
*
*	返 回 值:	SDT_OK				提取VDP信息成功
*				SDT_PT_NOT_EXIST	指针不存在
*				SDT_OUT_OF_BOUND	提取信息过程中访问越界
*/
SDT_RC sdsink_get_vdp_info(VDP_INFO_T* vdpInfoStruct, UINT8 * vdpBuffer, UINT32 vdpTailOffset)
{
	SDT_RC result = SDT_OK;
	UINT8* pos = NULL;

	if (PT_IS_NULL(vdpInfoStruct) || PT_IS_NULL(vdpBuffer))
	{
		SDT_DEBUG_MSG("[vdpInfoStruct || vdpBuffer ]pointer does not exist!\n");
		result = SDT_PT_NOT_EXIST;
		return result;
	}

	pos = vdpBuffer + vdpTailOffset;
	/* 提取reserved01 */ 
	vdpInfoStruct->m_reserved01 = LongFromChar(pos);
	pos += sizeof(vdpInfoStruct->m_reserved01);

	/* 提取reserved02 */ 
	vdpInfoStruct->m_reserved02 = ShortFromChar(pos);
	pos += sizeof(vdpInfoStruct->m_reserved02);

	/* 提取UDV*/ 
	vdpInfoStruct->m_UserDataVersion = ShortFromChar(pos);
	pos += sizeof(vdpInfoStruct->m_UserDataVersion);

	/* 提取SSC*/ 
	vdpInfoStruct->m_SafeSequCount = LongFromChar(pos);
	pos += sizeof(vdpInfoStruct->m_SafeSequCount);

	/* 提取SC*/ 
	vdpInfoStruct->m_SafetyCode = LongFromChar(pos);
	pos += sizeof(vdpInfoStruct->m_SafetyCode);
	
	/* 越界检测 */
	if (pos !=  (vdpBuffer + vdpTailOffset + SDT_TAIL_SIZE))
	{
		SDT_DEBUG_MSG("access out of bound!\n");
		result = SDT_OUT_OF_BOUND;
		return result;
	}
	

	return result;
}

/*
*	函数名称:	sdsink_check_vdp_type
*	函数功能:	判断接收到的VDP的类型
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_vdp_type(SDSINK_CHANNEL * pSdsinkChannel)
{
	/* 判断 VDP是否为正确VDP */
	sdsink_check_vdp_is_correct(pSdsinkChannel);

	/* 判断 VDP是否为重复VDP */
	sdsink_check_vdp_is_repeated(pSdsinkChannel);

	/* 判断 VDP是否为初始VDP */
	sdsink_check_vdp_is_initial(pSdsinkChannel);

	/* 判断 VDP是否为新VDP   */
	sdsink_check_vdp_is_new(pSdsinkChannel);

	/* 判断 VDP是否为有效VDP */
	sdsink_check_vdp_is_valid(pSdsinkChannel);

}

/*
*	函数名称:	sdsink_check_vdp_is_correct
*	函数功能:	判断VDP是否为正确VDP
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_vdp_is_correct(SDSINK_CHANNEL * pSdsinkChannel)
{
	/* 判断接收到的SC 和 通道A计算得到的SC是否相等 */
	if ((pSdsinkChannel->m_channelASC == pSdsinkChannel->m_VDPInfo.m_SafetyCode) &&
		(pSdsinkChannel->m_udv == pSdsinkChannel->m_VDPInfo.m_UserDataVersion))
	{
		/* 接收到的VDP是正确VDP */ 
		pSdsinkChannel->m_correctVDPFlag = SDT_TRUE;

		/* 当前通过通道A 接收到的VDP*/ 
		pSdsinkChannel->m_sdsrcIdentity = STA_SDSRC_CHANNEL_A;
		


	}
	/* 判断接收到的SC 和 通道B计算得到的SC是否相等 */
	else if ((pSdsinkChannel->m_channelBSC == pSdsinkChannel->m_VDPInfo.m_SafetyCode) &&
			 (pSdsinkChannel->m_udv == pSdsinkChannel->m_VDPInfo.m_UserDataVersion))
	{
		/* 接收到的VDP是正确VDP */
		pSdsinkChannel->m_correctVDPFlag = SDT_TRUE;
		
		/* 当前通过通道B 接收到的VDP*/
		pSdsinkChannel->m_sdsrcIdentity = STA_SDSRC_CHANNEL_B;

	}
	/* 接收到的VDP是错误的VDP */
	else
	{
		pSdsinkChannel->m_correctVDPFlag = SDT_FALSE;
		//printf("sc we save:%d sc we calculate:%d\n",pSdsinkChannel->m_VDPInfo.m_SafetyCode,pSdsinkChannel->m_channelASC);

	}


}

/*
*	函数名称:	sdsink_check_vdp_is_repeated
*	函数功能:	判断VDP是否为重复VDP
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_vdp_is_repeated(SDSINK_CHANNEL* pSdsinkChannel)
{
	UINT32 tempSSC = 0;
	/* 判断当前VDP是否为正确的VDP*/ 
	if (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag)
	{/* 当前接收到的VDP是正确的VDP */

		tempSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel);
		/* 判断 SSC是否和上一有效或初始VDP的SSC相同	*/
		if (tempSSC== pSdsinkChannel->m_VDPInfo.m_SafeSequCount)
		{
			pSdsinkChannel->m_repeatedVDPFlag = SDT_TRUE;
		}
		/* 当前VDP不是重复的VDP					*/
		else
		{
			pSdsinkChannel->m_repeatedVDPFlag = SDT_FALSE;
		}
	}
	else
	{/* 当前接收到的VDP不是正确的VDP */
		/* 当前VDP不是重复的VDP*/
		pSdsinkChannel->m_repeatedVDPFlag = SDT_FALSE;
	}
}

/*
*	函数名称:	sdsink_check_vdp_is_initial
*	函数功能:	判断VDP是否为初始VDP
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_vdp_is_initial(SDSINK_CHANNEL* pSdsinkChannel)
{
	UINT8  flag = 0;

	/* 判断 当前VDP是非重复且正确的 */ 
	if ((SDT_FALSE == pSdsinkChannel->m_repeatedVDPFlag) && (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag))
	{
		/*	判断 当前VDP是否是上电/重置后的第一个VDP */ 
		if (SDT_TRUE == pSdsinkChannel->m_powerOnResetFlag)
		{
			/* 当前VDP是初始VDP */ 
			pSdsinkChannel->m_initialVDPFlag = SDT_TRUE;

			/* 将上电重置标志位置为否 */
			pSdsinkChannel->m_powerOnResetFlag = SDT_FALSE;

			flag = 1;
		}
	
		/* 判断 当前VDP是否是宿时间丢失后收到的第一个VDP */ 
		if (SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom)
		{
			/* 当前VDP是初始VDP */
			pSdsinkChannel->m_initialVDPFlag = SDT_TRUE;

			flag = 1;

		}
	
		/* 判断是否发生发送端通道A/通道B切换 */  //??: 为什么不用初始SID判断
		if (pSdsinkChannel->m_lastSdsrcIdentity != pSdsinkChannel->m_sdsrcIdentity)
		{
			/* 当前VDP是初始VDP */
			pSdsinkChannel->m_initialVDPFlag = SDT_TRUE;

			flag = 1;
		}

		if (0 == flag)
		{
			pSdsinkChannel->m_initialVDPFlag = SDT_FALSE;
		}
	}
	else
	{
		pSdsinkChannel->m_initialVDPFlag = SDT_FALSE;
	}
}

/*
*	函数名称:	sdsink_check_vdp_is_new
*	函数功能:	判断VDP是否为新VDP
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_vdp_is_new(SDSINK_CHANNEL * pSdsinkChannel)
{
	/* 判断 当前VDP是是正确VDP且不是初始VDP */ 
	if ((SDT_TRUE == pSdsinkChannel->m_correctVDPFlag) && (SDT_FALSE == pSdsinkChannel->m_initialVDPFlag))
	{
		UINT32 tempSID = 0;
		UINT32 lastVDPSSC = 0;

		tempSID = sdsink_get_current_sid(pSdsinkChannel);		/* 获取当前的SID		*/ 
		lastVDPSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel);	/* 获取上一VDP的SSC		*/

		if (0 == tempSID)
		{
			SDT_DEBUG_MSG("SID equal 0, maybe something is error, please check configuation!\n");
		}

		/* 判断 SID和初始SID相同, 且接收序号需要落在接收窗口内*/
		if ((pSdsinkChannel->m_initialSID == tempSID) 
			&&(((lastVDPSSC + 1) <= pSdsinkChannel->m_VDPInfo.m_SafeSequCount) 
				&& ((lastVDPSSC + pSdsinkChannel->m_recvWindowSize) >= pSdsinkChannel->m_VDPInfo.m_SafeSequCount)))
		{
			/* 当前VDP是新VDP */
			pSdsinkChannel->m_newVDPFlag = SDT_TRUE;
		}
		else
		{
			pSdsinkChannel->m_newVDPFlag = SDT_FALSE;
		}
	}
	else
	{
		pSdsinkChannel->m_newVDPFlag = SDT_FALSE;
	}
}

/*
*	函数名称:	sdsink_check_vdp_is_valid
*	函数功能:	判断VDP是否为有效VDP
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_vdp_is_valid(SDSINK_CHANNEL * pSdsinkChannel)
{
	/* 判断 当前VDP是否是新VDP		*/ 
	if (SDT_TRUE == pSdsinkChannel->m_newVDPFlag)
	{
		pSdsinkChannel->m_validVDPFlag = SDT_TRUE;
	}
	/* 判断 当前VDP是新VDP的重复VDP 既 当前VDP正确且非初始, 且SSC和上一有效VDP相同*/ 
	else if ((SDT_TRUE == pSdsinkChannel->m_correctVDPFlag) &&
			 (SDT_FALSE == pSdsinkChannel->m_initialVDPFlag) &&
			 (pSdsinkChannel->m_lastValidVDPSSC == pSdsinkChannel->m_VDPInfo.m_SafeSequCount))
	{
		pSdsinkChannel->m_validVDPFlag = SDT_TRUE;
	}
	else
	{
		pSdsinkChannel->m_validVDPFlag = SDT_FALSE;
	}
}

/*
*	函数名称:	sdsink_get_last_VDP_SSC
*	函数功能:	如果上一VDP是初始VDP返回初始VDP的SSC, 否则返回上一有效VDP的SSC
*	参数说明:	UINT32		sdsinkHandle		接收端通道的句柄
*
*	返 回 值:	非0			有效的ssc
*				0			无效的ssc
*/
UINT32 sdsink_get_last_VDP_SSC(SDSINK_CHANNEL* pSdsinkChannel)
{
	UINT32 tempSSC = 0;
	tempSSC = pSdsinkChannel->m_lastValidVDPSSC;
	tempSSC >= pSdsinkChannel->m_initialVDPSSC ? pSdsinkChannel->m_lastValidVDPSSC : pSdsinkChannel->m_initialVDPSSC;

	if (0 == tempSSC)
	{
		SDT_DEBUG_MSG("SSC equal 0, maybe something is error, please check configuation!\n");
	}

	return tempSSC;
}

/*
*	函数名称:	sdsink_check_communication_safe_state
*	函数功能:	判断当前接收端通信安全状态
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_communication_safe_state(SDSINK_CHANNEL* pSdsinkChannel)
{
	/*	宿时间监视的通信安全判断	*/ 
	sdsink_check_rts_communication_safe(pSdsinkChannel);

	if (SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom)
	{
		sdsink_reset_vdp_parsing_environment(pSdsinkChannel);
	}
	else
	{
		sdsink_check_gtc_communication_safe(pSdsinkChannel);	/*	守护时间监视的通信安全判断	*/
		sdsink_check_lmc_communication_safe(pSdsinkChannel);	/*	延迟时间监视的通信安全判断	*/
		sdsink_check_cmc_communication_safe(pSdsinkChannel);	/*	通道监视的通信安全判断		*/
	}

	/*	返回接收端当前通信安全状态	*/ 
	if ((SDT_SAFE == pSdsinkChannel->m_gtcSafeCom) &&
		(SDT_SAFE == pSdsinkChannel->m_lmcSafeCom) &&
		(SDT_SAFE == pSdsinkChannel->m_cmcSafeCom))
	{
		pSdsinkChannel->m_safeComFlag = SDT_SAFE;
	}
	else
	{
		pSdsinkChannel->m_safeComFlag = SDT_UNSAFE;
	}
}

/*
*	函数名称:	sdsink_check_rts_communication_safe
*	函数功能:	宿时间监视的通信安全判断
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_rts_communication_safe(SDSINK_CHANNEL* pSdsinkChannel)
{
	/* 当前VDP为初始VDP或新VDP时, 更新宿时间监视计时器, 当前计时器	*/
	if (SDT_TRUE == pSdsinkChannel->m_initialVDPFlag)
	{
		pSdsinkChannel->m_rtsTimer = pSdsinkChannel->m_currentProcessStartTime;
	}
	else
	{
		/* 宿时间监视的通信安全计时器 维持上一状态的值, 无需处理 */
	}

	/* 判断是否发生超时 */
	if ( (SDT_INIT_VALUE != pSdsinkChannel->m_rtsTimer) && 
		 (SDT_INIT_VALUE != pSdsinkChannel->m_currentProcessStartTime)&&
		 ((SDT_CLOCK)pSdsinkChannel->m_rxSafe >= (pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_rtsTimer)))
	{
		/* 未超时 将宿时间监视的通道安全状态置为安全*/
		pSdsinkChannel->m_rtsSafeCom = SDT_SAFE;
	}
	else
	{
		/* 超时 将宿时间监视的通道安全状态置为不安全 */
		pSdsinkChannel->m_rtsSafeCom = SDT_UNSAFE;
	}
	
	if(SDT_TRUE == pSdsinkChannel->m_newVDPFlag)
	{
		pSdsinkChannel->m_rtsTimer = pSdsinkChannel->m_currentProcessStartTime;
	}
	else
	{}
	
	
	/*
	printf("/-*******************宿时间监控测试信息*******************-/\n");
	printf("初始VDP: %s\n", SDT_TRUE == pSdsinkChannel->m_initialVDPFlag?"是":"否");
	printf("新  VDP: %s\n", SDT_TRUE == pSdsinkChannel->m_newVDPFlag ? "是" : "否");
	printf("rts计时器: %u\n", pSdsinkChannel->m_rtsTimer);
	printf("当前进程计时器: %u\n", pSdsinkChannel->m_currentProcessStartTime);
	printf("宿时间监控状态: %s\n", SDT_SAFE == pSdsinkChannel->m_rtsSafeCom ? "安全" : "不安全");
	printf("/-*******************宿时间监控测试信息*******************-/\n");
	*/
	
}

/*
*	函数名称:	sdsink_check_gtc_communication_safe
*	函数功能:	守护时间监视的通信安全判断
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_gtc_communication_safe(SDSINK_CHANNEL*  pSdsinkChannel)
{
	UINT32 tempSID = sdsink_get_current_sid(pSdsinkChannel);		/* 获取当前VDP的SID */ 
	if (0 == tempSID)
	{
		SDT_DEBUG_MSG("SID equal 0, maybe something is error, please check configuation!\n");
	}
	/*printf("/-*******************守护时间监控测试信息*******************-/\n");
	printf("当前接收端通道: %s\n", STA_SDSRC_CHANNEL_A == pSdsinkChannel->m_sdsrcIdentity ? "A" : "B");
	printf("接收端A通道SC: %x\n", pSdsinkChannel->m_channelASC);
	printf("接收端B通道SC: %x\n", pSdsinkChannel->m_channelBSC);
	printf("接收到VDP的SC: %x\n", pSdsinkChannel->m_VDPInfo.m_SafetyCode);
	printf("计时器时间: %u\n", pSdsinkChannel->m_gtcTimer);
	printf("计时器过期剩余时间: %d\n", (SDT_CLOCK)pSdsinkChannel->m_tGuard - (pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_gtcTimer));
	printf("初始SID: %u\n", pSdsinkChannel->m_initialSID);
	printf("当前SID: %u\n", sdsink_get_current_sid(pSdsinkChannel));
	printf("/-*******************守护时间监控测试信息*******************-/\n");*/



	if (SDT_INIT_VALUE == pSdsinkChannel->m_initialSID)
	{
		pSdsinkChannel->m_gtcSafeCom = SDT_UNSAFE;
		pSdsinkChannel->m_gtcTimer = pSdsinkChannel->m_currentProcessStartTime;
	}
	/* 发生了通道切换 */
	else if ((SDT_INIT_VALUE != pSdsinkChannel->m_initialSID) &&
		(tempSID != pSdsinkChannel->m_initialSID))
	{
		if (SDT_INIT_VALUE == pSdsinkChannel->m_gtcTimer)
		{
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
		}
		else
		{
			pSdsinkChannel->m_gtcSafeCom = SDT_UNSAFE;
		}
		pSdsinkChannel->m_gtcTimer = pSdsinkChannel->m_currentProcessStartTime;
	}
	/* 没有发生通道切换 */
	else if ((SDT_INIT_VALUE != pSdsinkChannel->m_initialSID) &&
		(tempSID == pSdsinkChannel->m_initialSID))
	{
		if (SDT_TRUE == pSdsinkChannel->m_gtcResetFlag)
		{
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
			pSdsinkChannel->m_gtcResetFlag = SDT_FALSE;
		}
		else
		{
			/* 无处理 */
		}

		if (SDT_INIT_VALUE == pSdsinkChannel->m_gtcTimer)
		{
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
		}
		else if ((SDT_CLOCK)pSdsinkChannel->m_tGuard < (pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_gtcTimer))
		{
			pSdsinkChannel->m_gtcTimer = SDT_INIT_VALUE;
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
		}
		else
		{
			/* 无处理 */
		}
	}
}

/*
*	函数名称:	sdsink_check_lmc_communication_safe
*	函数功能:	时延监视的通信安全判断
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_lmc_communication_safe(SDSINK_CHANNEL* pSdsinkChannel)
{
	UINT32 tempSSC = 0;

	/* 更新计时器 基准SSC 计算预期SSC */ //??: 是否在宿时间丢失情况下接收到的第一个VDP才更新基准SSC , 为了避免主备机切换接收到初始VDP导致的定时器更新
	if (((SDT_TRUE == pSdsinkChannel->m_lmcResetFlag) && (SDT_TRUE == pSdsinkChannel->m_initialVDPFlag))
			/*|| (pSdsinkChannel->m_currentProcessStartTime-pSdsinkChannel->m_lmcTimer >30*pSdsinkChannel->m_txPeriod)*/)
	{
		/* 当前VDP为初始VDP 		*/
		/* 重置 时延监视计时器		*/
		pSdsinkChannel->m_lmcTimer = pSdsinkChannel->m_currentProcessStartTime;
		/* 重置 基准SSC				*/
		pSdsinkChannel->m_BaseSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;
		/* 重置 预期SSC				*/
		pSdsinkChannel->m_ExceptSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;
	}
	else
	{
		/* 当前VDP不是初始VDP */
		SDT_CLOCK differTime = pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_lmcTimer;		/* 计算预期SSC(向上取整)	*/
		pSdsinkChannel->m_ExceptSSC = pSdsinkChannel->m_BaseSSC + (UINT32)((differTime + pSdsinkChannel->m_txPeriod - 1) / pSdsinkChannel->m_txPeriod);	

		pSdsinkChannel->m_lmcResetFlag = SDT_FALSE;
	}

	if ( SDT_INIT_VALUE != pSdsinkChannel->m_BaseSSC)
	{
		if ((SDT_TRUE == pSdsinkChannel->m_validVDPFlag) || (SDT_TRUE == pSdsinkChannel->m_initialVDPFlag))
		{
			tempSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;
		}
		else
		{
			tempSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel) + 1;	
		}

		/* 计算 SSC的差值 */
		UINT32 diffSSC = pSdsinkChannel->m_ExceptSSC >= tempSSC ? (pSdsinkChannel->m_ExceptSSC - tempSSC) : (tempSSC - pSdsinkChannel->m_ExceptSSC);

		/* 判断 是否发生延迟 */ 
		if ((diffSSC * pSdsinkChannel->m_txPeriod) >= pSdsinkChannel->m_rxSafe)
		{
			/* 接收发生延迟, 通信安全状态置为不安全 */
			pSdsinkChannel->m_lmcSafeCom = SDT_UNSAFE;
		}
		else
		{
			/* 接收未发生延迟, 通信安全状态置为安全 */
			pSdsinkChannel->m_lmcSafeCom = SDT_SAFE;
		}
	}
	else
	{
		/* 当 基准SSC不存在时 意味者没有接收到VDP 将安全状态置为不安全 */ 
		pSdsinkChannel->m_lmcSafeCom = SDT_SAFE;
	}
	if(0/*75>pSdsinkChannel->m_txPeriod*/)
	{
		printf("LMC info: \t");
		printf("SSCi : %u\t", pSdsinkChannel->m_BaseSSC);
		printf("SSC : %u\t", pSdsinkChannel->m_VDPInfo.m_SafeSequCount);
		printf("SSCE : %u\t", pSdsinkChannel->m_ExceptSSC);
		printf("SSCdiffer : %u\n", pSdsinkChannel->m_ExceptSSC >= pSdsinkChannel->m_VDPInfo.m_SafeSequCount ? (pSdsinkChannel->m_ExceptSSC - pSdsinkChannel->m_VDPInfo.m_SafeSequCount) : (pSdsinkChannel->m_VDPInfo.m_SafeSequCount - pSdsinkChannel->m_ExceptSSC));
	}

	
}

/*
*	函数名称:	sdsink_check_cmc_communication_safe
*	函数功能:	通道监视的通信安全判断
*	参数说明:	SDSINK_CHANNEL*		pSdsinkChannel		接收端通道指针
*
*	返 回 值:	无
*/
void sdsink_check_cmc_communication_safe(SDSINK_CHANNEL* pSdsinkChannel)
{
	/* 计算正确计数/错误计数 */ 
	if (SDT_FALSE == pSdsinkChannel->m_repeatedVDPFlag)
	{
		/* 排除重复VDP的情况	*/
		/* 判断 当前VDP是否正确	*/
		if (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag)
		{
			/* 当前VDP正确, 错误计数减1, 下限为0*/
			pSdsinkChannel->m_wrongVDPCounter = (0 < pSdsinkChannel->m_wrongVDPCounter ? (pSdsinkChannel->m_wrongVDPCounter - 1) : 0);
		}
		else
		{
			/* 当前VDP错误, 错误计数增长一个步长, 上限为错误计数上限m_wrongCounterUpper*/
			pSdsinkChannel->m_wrongVDPCounter =
				((pSdsinkChannel->m_wrongVDPCounter + pSdsinkChannel->m_wrongIncreaseStep) < pSdsinkChannel->m_wrongCounterUpper ?
				(pSdsinkChannel->m_wrongVDPCounter + pSdsinkChannel->m_wrongIncreaseStep) :
				pSdsinkChannel->m_wrongCounterUpper);
		}
	}

	/* 通道安全状态判断 */ 
	if (pSdsinkChannel->m_cmThr <= pSdsinkChannel->m_wrongVDPCounter)
	{
		/* 达到错误阈值, 通道监视通信安全状态 置为不安全	*/
		pSdsinkChannel->m_cmcSafeCom = SDT_UNSAFE;
	}
	else if (0 == pSdsinkChannel->m_wrongVDPCounter)
	{
		/* 错误计数归0, 通道监视通信安全状态 置为安全		*/ 
		pSdsinkChannel->m_cmcSafeCom = SDT_SAFE;
	}
}

/*
*	函数名称:	sdsink_get_current_sid
*	函数功能:	获取当前的SID
*	参数说明:	UINT8		sdsinkHandle		接收端通道句柄
*
*	返 回 值:	UINT32		SID					返回当前接收通道的SID
*				UINT32		0					当前VDP不正确, 返回0
*/
UINT32 sdsink_get_current_sid(SDSINK_CHANNEL* pSdsinkChannel)
{
	UINT32 tempSid = 0;

	if (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag)
	{
		tempSid = (STA_SDSRC_CHANNEL_A == pSdsinkChannel->m_sdsrcIdentity ? pSdsinkChannel->m_channelASID : pSdsinkChannel->m_channelBSID);
	}

	return tempSid;
}


// 80个通道保存并打印测试信息
void sdsink_save_test_info(SDT_HANDLE sdsinkHandle)
{
	
	/*static unsigned long lastTime[SDSINK_CHANNEL_AMOUNT_MAX] = { 0 };
	static unsigned long parse_interval[SDSINK_CHANNEL_AMOUNT_MAX] = { 0 };//一个通道两次处理数据之间的间隔
	static UINT32 dataLenth[SDSINK_CHANNEL_AMOUNT_MAX] = { 0 };//解析后数据长度*/
	P_SDSINK_CHANNEL pSdsinkChannel = NULL;
	pSdsinkChannel = &(g_sdsink.channelArray[sdsinkHandle]);
	/*parse_interval[sdsinkHandle] =pSdsinkChannel->m_currentProcessStartTime - lastTime[sdsinkHandle];
	lastTime[sdsinkHandle] = pSdsinkChannel->m_currentProcessStartTime;
	dataLenth[sdsinkHandle]=vpdLength;*/

	VOS_ERR_T err;
	err=vos_mutexLock(pmyPrintMutex);
	printf("Handle : %01d\t", sdsinkHandle);
	printf("SMI : %04d\t", pSdsinkChannel->m_channelASMI);
	printf("SSC : %04d\t",  pSdsinkChannel->m_VDPInfo.m_SafeSequCount);
	printf("%s\t %s\t", pSdsinkChannel->m_correctVDPFlag == SDT_TRUE ? "CORRECT" : "correct", pSdsinkChannel->m_repeatedVDPFlag == SDT_TRUE ? "REPEAT" : "repeat");
	printf("%s\t%s\t", pSdsinkChannel->m_initialVDPFlag == SDT_TRUE ? "INITIAL" : "initial", pSdsinkChannel->m_newVDPFlag == SDT_TRUE ? "NEW" : "new");
	printf("%s\t", pSdsinkChannel->m_validVDPFlag == SDT_TRUE ? "valid" : "invalid");

	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_safeComFlag ? " SAFE " : "UNSAFE");
	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_rtsSafeCom ? "RTS" : "rts");
	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_gtcSafeCom ? "GTC" : "gtc");
	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_lmcSafeCom ? "LMC" : "lmc");
	printf("%s\n", SDT_SAFE == pSdsinkChannel->m_cmcSafeCom ? "CMC" : "cmc");
	err=vos_mutexUnlock(pmyPrintMutex);
}
/*
*	函数名称:	print_counters
*	函数功能:	打印指定通道的SDSINK质量统计量至log
*	参数说明:	UINT8		sdsinkHandle		接收端通道句柄
*/
void print_counters(SDT_HANDLE sdsinkHandle)
{
	P_SDSINK_CHANNEL pSdsinkChannel = NULL;
	pSdsinkChannel = &(g_sdsink.channelArray[sdsinkHandle]);
	/*VOS_ERR_T err;
	err=vos_mutexLock(pmyPrintMutex);
	printf("TLG NUMBER:%04d\t", pSdsinkChannel->m_channelASMI);
	printf("NEW: %u\t", pSdsinkChannel->m_statisticInfo.m_newVDPCounter);
	printf("REPEAT%u\t", pSdsinkChannel->m_statisticInfo.m_repeatedVDPVounter);
	printf("WRONG: %u\t", pSdsinkChannel->m_statisticInfo.m_errorVDPCounter);
	printf("REDUNDANT SWICH: %u\t", pSdsinkChannel->m_statisticInfo.m_switchChannelCounter);
	printf("OUT OF ORDER: %u\t", pSdsinkChannel->m_statisticInfo.m_wrongOrderCounter);
	printf("DELAY CYCLES: %u\t", pSdsinkChannel->m_statisticInfo.m_lantencyCounter);
	printf("UDVERR: %u\t", pSdsinkChannel->m_statisticInfo.m_wrongUDVCounter);
	printf("CMC UNSAFE: %u\n", pSdsinkChannel->m_statisticInfo.m_cmcUnsafeCounter);
	err=vos_mutexUnlock(pmyPrintMutex);*/
	
	
	
	//vos_printLog(VOS_LOG_ERROR,"AMOUNT:%03u\t",pSdsinkChannel->m_statisticInfo.m_VDPAmountCounter);
	//vos_printLog(VOS_LOG_ERROR,"AVAILABLE:%03u\t",pSdsinkChannel->m_statisticInfo.m_availableVDPCounter);
	//vos_printLog(VOS_LOG_ERROR, "WRONG: %u\t", pSdsinkChannel->m_statisticInfo.m_errorVDPCounter);
	//vos_printLog(VOS_LOG_ERROR, "REDUNDANT SWICH: %u\t", pSdsinkChannel->m_statisticInfo.m_switchChannelCounter);
	//vos_printLog(VOS_LOG_ERROR, "UDVERR: %u\t", pSdsinkChannel->m_statisticInfo.m_wrongUDVCounter);
	//vos_printLog(VOS_LOG_ERROR, "CMC UNSAFE: %u\n", pSdsinkChannel->m_statisticInfo.m_cmcUnsafeCounter);
	vos_printLog(VOS_LOG_ERROR, "sdsinkHandle:%02d\tNEW: %03u\tREPEAT%03u\tINVALID: %03u\tOUT OF ORDER: %03u\tDELAY CYCLES: %03u\tBaceSSC: %04u\tExceptSSC: %04u\tSSC: %04u\n"
			, sdsinkHandle, pSdsinkChannel->m_statisticInfo.m_newVDPCounter, pSdsinkChannel->m_statisticInfo.m_repeatedVDPVounter
			, pSdsinkChannel->m_statisticInfo.m_invalidVDPCounter, pSdsinkChannel->m_statisticInfo.m_wrongOrderCounter
			, pSdsinkChannel->m_statisticInfo.m_lantencyCounter, pSdsinkChannel->m_BaseSSC, pSdsinkChannel->m_ExceptSSC
			, pSdsinkChannel->m_VDPInfo.m_SafeSequCount);
	/*vos_printLog(VOS_LOG_ERROR, "sdsinkHandle:%02d\t", sdsinkHandle);
	vos_printLog(VOS_LOG_ERROR, "NEW: %03u\t", pSdsinkChannel->m_statisticInfo.m_newVDPCounter);
	vos_printLog(VOS_LOG_ERROR, "REPEAT%03u\t", pSdsinkChannel->m_statisticInfo.m_repeatedVDPVounter);
	vos_printLog(VOS_LOG_ERROR, "INVALID: %03u\t", pSdsinkChannel->m_statisticInfo.m_invalidVDPCounter);
	vos_printLog(VOS_LOG_ERROR, "OUT OF ORDER: %03u\t", pSdsinkChannel->m_statisticInfo.m_wrongOrderCounter);
	vos_printLog(VOS_LOG_ERROR, "DELAY CYCLES: %03u\t", pSdsinkChannel->m_statisticInfo.m_lantencyCounter);
	vos_printLog(VOS_LOG_ERROR, "BaceSSC: %04u\t", pSdsinkChannel->m_BaseSSC);
	vos_printLog(VOS_LOG_ERROR, "ExceptSSC: %04u\t", pSdsinkChannel->m_ExceptSSC);
	vos_printLog(VOS_LOG_ERROR, "SSC: %04u\n", pSdsinkChannel->m_VDPInfo.m_SafeSequCount);*/
	
	
}


/* 重置sdsink 上电/重启后调用该函数, 宿时间监视发生不安全后调用该函数 */
void sdsink_reset_vdp_parsing_environment(SDSINK_CHANNEL* pSdsinkChannel)
{
	pSdsinkChannel->m_powerOnResetFlag = SDT_TRUE;
	pSdsinkChannel->m_gtcResetFlag		= SDT_TRUE;		
	pSdsinkChannel->m_lmcResetFlag		= SDT_TRUE;

	/* vdp类型标志位全部置否 */
	pSdsinkChannel->m_correctVDPFlag	= SDT_FALSE;
	pSdsinkChannel->m_repeatedVDPFlag	= SDT_FALSE;
	pSdsinkChannel->m_initialVDPFlag	= SDT_FALSE;
	pSdsinkChannel->m_newVDPFlag		= SDT_FALSE;
	pSdsinkChannel->m_validVDPFlag		= SDT_FALSE;
	pSdsinkChannel->m_lastVDPIsNewFlag	= SDT_FALSE;
	
	/* vdp解析数据全部清零 */	
	
	memset((void*)&(pSdsinkChannel->m_VDPInfo), 0, sizeof(VDP_INFO_T));
	pSdsinkChannel->m_channelASC = SDT_INIT_VALUE;
	pSdsinkChannel->m_channelBSC = SDT_INIT_VALUE;
	pSdsinkChannel->m_sdsrcIdentity		= STA_SDSRC_CHANNEL_A; /* ??: 这里置为通道A 需要验证*/

	/* 安全状态全部置不安全 */
	
	pSdsinkChannel->m_safeComFlag	= SDT_UNSAFE;
	pSdsinkChannel->m_rtsSafeCom	= SDT_UNSAFE;
	pSdsinkChannel->m_gtcSafeCom	= SDT_UNSAFE;
	pSdsinkChannel->m_lmcSafeCom	= SDT_UNSAFE;
	pSdsinkChannel->m_cmcSafeCom	= SDT_UNSAFE;
	
	/* 通道历史记录全部置零  */
	
	pSdsinkChannel->m_lastSdsrcIdentity = STA_SDSRC_CHANNEL_A; 
	/* 重置后如果收到的第一条消息来自B机，则静态计数值中的切换次数会多一次（虽然并没有发生切换），其他功能不受影响 */
	/* 使用上一身份进行判断除初始化之外，仅在初始vdp判断时进行，判断不受m_lastValidVDPSSC未赋值影响 */
	pSdsinkChannel->m_lastValidVDPSSC	= SDT_INIT_VALUE;
	pSdsinkChannel->m_initialVDPSSC		= SDT_INIT_VALUE;
	pSdsinkChannel->m_initialSID		= SDT_INIT_VALUE;
	pSdsinkChannel->m_rtsTimer			= SDT_INIT_VALUE;
	pSdsinkChannel->m_gtcTimer			= SDT_INIT_VALUE;
	pSdsinkChannel->m_lmcTimer			= SDT_INIT_VALUE;
	pSdsinkChannel->m_BaseSSC			= SDT_INIT_VALUE;
	pSdsinkChannel->m_ExceptSSC			= SDT_INIT_VALUE;
//	pSdsinkChannel->m_wrongVDPCounter	= SDT_INIT_VALUE;/* ??: 用于通道监视的错误计数是否应该重置 */
	
}

/* 更新统计信息 */
UINT32 sdsink_update_statistic_info(SDSINK_CHANNEL* pSdsinkChannel, SDT_HANDLE sdsinkHandle)
{
	UINT32 result = 0;
	UINT32 tempSSC;
	static UINT32 s_counterETlg[SDSINK_CHANNEL_AMOUNT_MAX] = { 0 };

	/* 未接收到数据 退出*/
	if (SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom)
	{
		return result;
	}
	if(5000==s_counterETlg[sdsinkHandle])
		s_counterETlg[sdsinkHandle]=0;
	s_counterETlg[sdsinkHandle]++;
	
	/* 更新 新VDP计数器*/
	if (SDT_TRUE == pSdsinkChannel->m_newVDPFlag)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_newVDPCounter);
	}

	/* 更新 错误VDP计数器*/
	if (SDT_FALSE == pSdsinkChannel->m_correctVDPFlag)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_errorVDPCounter);
	}

	/* 更新 通道切换计数器*/
	if (pSdsinkChannel->m_lastSdsrcIdentity != pSdsinkChannel->m_sdsrcIdentity)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_switchChannelCounter);
	}
	/* 更新 乱序接收计数器*/
	/* 不应当对重复VDP进行统计 */
	tempSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel);
	if ((SDT_FALSE == pSdsinkChannel->m_repeatedVDPFlag) 
		&&(0 != tempSSC)
		&&((tempSSC + 1) != pSdsinkChannel->m_VDPInfo.m_SafeSequCount))
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_wrongOrderCounter);
	}

	/* 更新 重复VDP计数器*/
	if (SDT_TRUE == pSdsinkChannel->m_repeatedVDPFlag)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_repeatedVDPVounter);
	}

	/* 更新 发生延迟接收周期计数器*/
	if ((SDT_FALSE == pSdsinkChannel->m_initialVDPFlag)&&(SDT_UNSAFE == pSdsinkChannel->m_lmcSafeCom))
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_lantencyCounter);
	}

	/* 更新 错误UDV计数器*/
	if (pSdsinkChannel->m_udv != pSdsinkChannel->m_VDPInfo.m_UserDataVersion)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_wrongUDVCounter);
	}

	/* 更新 cmc非安全周期计数器 */
	if (SDT_UNSAFE == pSdsinkChannel->m_cmcSafeCom)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_cmcUnsafeCounter);
	}
	
	//测试用的一些计数值标准里没有后续可以去掉counter for test
	if (SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom)
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_rtsUnsafeCounter);
	
	SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_VDPAmountCounter);
	
	if ((SDT_SAFE == pSdsinkChannel->m_safeComFlag)&&(SDT_TRUE == pSdsinkChannel->m_validVDPFlag))
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_availableVDPCounter);
	if (SDT_TRUE != pSdsinkChannel->m_validVDPFlag)
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_invalidVDPCounter);
	
	return s_counterETlg[sdsinkHandle];
	
}

/* 重置统计信息 */
void sdsink_reset_statistic_info(SDSINK_STATISTIC_INFO_T* pStatInfo)
{
	pStatInfo->m_newVDPCounter			= SDT_INIT_VALUE;
	pStatInfo->m_errorVDPCounter		= SDT_INIT_VALUE;
	pStatInfo->m_switchChannelCounter	= SDT_INIT_VALUE;
	pStatInfo->m_wrongOrderCounter		= SDT_INIT_VALUE;
	pStatInfo->m_repeatedVDPVounter		= SDT_INIT_VALUE;
	pStatInfo->m_lantencyCounter		= SDT_INIT_VALUE;
	pStatInfo->m_wrongUDVCounter		= SDT_INIT_VALUE;
	pStatInfo->m_cmcUnsafeCounter		= SDT_INIT_VALUE;
	
	pStatInfo->m_rtsUnsafeCounter		= SDT_INIT_VALUE;
	pStatInfo->m_VDPAmountCounter		= SDT_INIT_VALUE;
	pStatInfo->m_availableVDPCounter	= SDT_INIT_VALUE;
	pStatInfo->m_invalidVDPCounter		= SDT_INIT_VALUE;
}
