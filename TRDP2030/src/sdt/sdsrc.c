#include "sdsrc.h"
#include "sdt_utils.h"

/*	---------------------------------------内部变量定义--------------------------------------------------	*/
/*  发送端定义  */
SDSRC g_sdsrc = {.channelCapacity = SDSRC_CHANNEL_AMOUNT_MAX};		/* 全局变量, 保存当前接发送端通道的信息 */
/*  测试时使用的变量  */
INT32 test_cycCount = 0;
UINT8 test_stepCount=0;


/*	---------------------------------------内部函数定义--------------------------------------------------	*/
/* 配置发送端通道 */
SDT_RC sdsrc_config_sdsrc_channel(SDSRC_CHANNEL* pSdsrcChannel, SDT_CONFIG_T const * pSdsrcConfig);

/* 添加VDP尾部							*/ 
SDT_RC sdsrc_add_vdp_tail(UINT8* vdpBuffer, UINT32 tailOffset, VDP_INFO_T* pVDPInfo, UINT32 sid);

/* 检测句柄有效性						*/ 
//SDT_RC sdsrc_check_handle_valide(SDT_HANDLE sdsrcHandle);

/* 处理传入vpd数据的长度为四字节倍数	*/ 
UINT32 sdsrc_four_byte_alignment(UINT32 length);

/* 打印vpd信息							*/
void sdsrc_printf_test_info(P_SDSRC_CHANNEL const pSdsrcChannel, UINT8 const* vpdBuffer, UINT32 const vpdBufferLength);



/*	-----------------------------------------接口实现---------------------------------------------------	*/
/*
*	函数名称:	sdsrc_ssc_add_one
*	函数功能:	ssc值加上入参
*/
SDT_RC sdsrc_SSCvalue_add_one(SDT_HANDLE sdsrcHandle,UINT32 value)
{
	SDT_RC result=SDT_OK;
	/*	判断传入的句柄是否合理	*/ 
	if (SDSRC_HANDLE_IS_INVALIDE(sdsrcHandle))
	{
		SDT_DEBUG_MSG("invalid sdsrc handle! \n");
		result = SDT_INVALID_HANDLE;
		return result;
	}
	
	/*  SSC加上值  */
	g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount=g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount+value;

	return result;
}
/*
*	函数名称:	sdsrc_SSCvalue_add_one
*	函数功能:	创建 并 配置 发送端通道
*
*	参数说明:	SDT_HANDLE*			pSdsrcHandle	发送端句柄指针
*				SDT_CONFIG_T*		pSdsrcConfig	发送端通道配置信息
*
*	返 回 值:	SDT_OK					通道创建成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_PARAMETER	无效参数
*				SDSRC_CHANNEL_FULL		通道数量达到上限
*				SDT_INVALID_SID			SID计算错误
*
*/
SDT_RC sdsrc_create_channel(SDT_HANDLE* pSdsrcHandle, SDT_CONFIG_T* pSdsrcConfig)
{
	SDT_RC			result = SDT_OK;
	SDT_HANDLE		tempHandle = g_sdsrc.channelNum;	/* 获取发送端通道第一个未被占用的下标 */
	P_SDSRC_CHANNEL tempSdsrcChannel = NULL;

	/* 判断指针是否存在 */
	if (PT_IS_NULL(pSdsrcHandle) || PT_IS_NULL(pSdsrcConfig))
	{
		SDT_DEBUG_MSG("sdsrc channel create error, pointer does not exist!\n");
		result = SDT_PT_NOT_EXIST;
		return result;
	}
	
	/* 判断传入的发送端参数是否合理	*/	
	result = sdt_check_config_valid(pSdsrcConfig);
	if (SDT_OK != result)
	{
		SDT_DEBUG_MSG("sdsrc channel create failed, configuation parameters are invalide!\n");
		return result;
	}

	/* 申请通道 */
	if (tempHandle >= g_sdsrc.channelCapacity)
	{
		SDT_DEBUG_MSG("sdsrc channel create failed, channel array is full!\n");
		result = SDSRC_CHANNEL_FULL;	/* 当前使用通道数量达到上限 */
		return result;
	}

	/* 获取发送端通 */
	tempSdsrcChannel = &(g_sdsrc.channelArray[tempHandle]);
	if (PT_IS_NULL(tempSdsrcChannel))
	{
		SDT_DEBUG_MSG("sdsrc create failed, can not get channel pointer!");
		result = SDT_PT_NOT_EXIST;		/* 未获取到发送端通道指针 */
		return result;
	}
	else
	{
		result = sdsrc_config_sdsrc_channel(tempSdsrcChannel, pSdsrcConfig);	/* 配置当前通道 */
	}
	
	if (SDT_OK != result)
	{
		memset(tempSdsrcChannel, 0, sizeof(SDSRC_CHANNEL));
		SDT_DEBUG_MSG("SDSRC : channel %u create failed! \n", tempHandle);
	}
	else
	{
		g_sdsrc.channelNum++;			/* 配置完成,通道数量增加	*/
		*pSdsrcHandle = tempHandle;		/* 返回句柄值				*/
		SDT_DEBUG_MSG("SDSRC : channel %u create success! \n", tempHandle);
	}

	return result;
}


/*
*	函数名称:	sdsrc_generate_VDP
*	函数功能:	发送端数据处理
*	
*	参数说明:	SDT_HANDLE		sdsrcHandle		发送端通道句柄
*				UINT8*			vpdBuffer		vpd信息缓存
*				UINT32			vpdLength		vpd信息长度
*				UINT8*			vdpBuffer		vdp信息缓存
*				UINT32*			vdpLength		vdp信息长度
* 
*	返 回 值:	SDT_OK					VDP生成成功
*				SDT_INVALID_HANDLE		句柄无效
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_VPD_LEN		VPD长度无效
*				SDT_OUT_OF_BOUND		访问越界
* 
*/
SDT_RC sdsrc_generate_VDP(SDT_HANDLE sdsrcHandle, UINT8* vpdBuffer, UINT32 vpdLength, UINT8* vdpBuffer, UINT32* vdpLength)
{
	SDT_RC	result = SDT_OK;
	UINT32	tempVpdLength = 0;
	UINT32	vdpSize = 0;
	UINT8	tempVdpBuffer[SDT_VPDMAX+SDT_TAIL_SIZE];		/* 984+16 = 1000 字节*/
	memset(tempVdpBuffer, 0, (size_t)(SDT_VPDMAX + SDT_TAIL_SIZE));

	/*	判断传入的句柄是否合理	*/ 
	if (SDSRC_HANDLE_IS_INVALIDE(sdsrcHandle))
	{
		SDT_DEBUG_MSG("invalid sdsrc handle! \n");
		result = SDT_INVALID_HANDLE;
		return result;
	}

	/*	检测传入的指针是否存在	*/
	if (PT_IS_NULL(vpdBuffer) || PT_IS_NULL(vdpBuffer) || PT_IS_NULL(vdpLength))
	{
		SDT_DEBUG_MSG("[vpd buffer || vdp buffer || vdp length]pointer does not exist!");
		result = SDT_PT_NOT_EXIST;
		return result;
	}
	

	/*  检测传入的长度是否合理  */
	if ((SDT_VPDMIN >= vpdLength) || (SDT_VPDMAX < vpdLength))
	{
		result = SDT_INVALID_VPD_LEN;
		SDT_DEBUG_MSG("vpd length is unreasonable!");
		return result;
	}
	
	/* vpd数据长度 32位对齐 */
	tempVpdLength = sdsrc_four_byte_alignment(vpdLength);
	
	/*  拷贝VPD数据到VDP缓存	*/
	memcpy(tempVdpBuffer, (const void*)vpdBuffer, vpdLength);
	
	/*   添加VDP尾				*/	
	result = sdsrc_add_vdp_tail(tempVdpBuffer, tempVpdLength, &(g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo), g_sdsrc.channelArray[sdsrcHandle].m_SID);
	if (SDT_OK != result)
	{
		SDT_DEBUG_MSG("vdp tail add failed!\n");
		return result;
	}

	/*	返回数据	*/ 
	//返回的vpd长度是纯用户数据的长度再加上16
	vdpSize = vpdLength + SDT_TAIL_SIZE;
	*vdpLength = vdpSize;
	//首先将用户数据部分拷贝到vdpBuffer中，所使用的长度是未经四字节对齐处理的vpd长度，也就是纯用户数据部分
	memcpy((void*)vdpBuffer, tempVdpBuffer, vpdLength);
	//然后将vdp尾部16个字节的校验数据拷贝到vdpBuffer中，其中vdpBuffer的偏移是刚才的vpd长度，tempvdpBuffer的偏移是经过四字节对齐处理后的vpd长度
	memcpy(vdpBuffer+vpdLength, (const void *)(tempVdpBuffer+tempVpdLength), SDT_TAIL_SIZE);

#ifdef _TEST_INFO
	sdsrc_printf_test_info(&(g_sdsrc.channelArray[sdsrcHandle]), vpdBuffer, vpdLength);
#endif

	/*  递增SSC  */
	g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount++;

	return result;
}


/*	-----------------------------内部函数实现--------------------------------------------------	*/

/* 配置发送端通道 */
SDT_RC sdsrc_config_sdsrc_channel(SDSRC_CHANNEL* pSdsrcChannel, SDT_CONFIG_T const * pSdsrcConfig)
{
	SDT_RC result = SDT_OK;

	/*	将通道状态标志位 置为激活		*/
	pSdsrcChannel->m_handleEffective = SDT_EFFECTIVE;

	/*	配置时间参数					*/
	pSdsrcChannel->m_rxPeriod = pSdsrcConfig->rxPeriod;
	pSdsrcChannel->m_txPeriod = pSdsrcConfig->txPeriod;

	/*  配置SMI */
	if (STA_SDSRC_CHANNEL_A == pSdsrcConfig->channelIdentity)
	{
		pSdsrcChannel->m_SMI = pSdsrcConfig->smi1;
	}
	else if (STA_SDSRC_CHANNEL_B == pSdsrcConfig->channelIdentity)
	{
		pSdsrcChannel->m_SMI = pSdsrcConfig->smi2;
	}
	else
	{
		SDT_DEBUG_MSG("channel identity error! sdsrc identity should be [STA_SDSRC_CHANNEL_A|STA_SDSRC_CHANNEL_B]\n");
		result = SDT_INVALID_PARAMETER;
		return result;
	}

	/*	设置SID结构体参数			*/
	memset(&(pSdsrcChannel->m_SIDInfo), 0, sizeof(SID_INFO_T));
	pSdsrcChannel->m_SIDInfo.SDTProtVers = SDT_VERSION;

	/*  计算发送端通道的SID         */
	pSdsrcChannel->m_SID = sdt_calculate_sid(&(pSdsrcChannel->m_SIDInfo), pSdsrcChannel->m_SMI);
	if (0 == pSdsrcChannel->m_SID)
	{
		result = SDT_INVALID_SID;
		return result;
	}

	/*  设置VDP结构体参数			*/
	memset(&(pSdsrcChannel->m_VDPInfo), 0, sizeof(VDP_INFO_T));
	pSdsrcChannel->m_VDPInfo.m_UserDataVersion = pSdsrcConfig->udv;
	
	pSdsrcChannel->m_VDPInfo.m_SafeSequCount = 1u;
	//pSdsrcChannel->m_VDPInfo.m_SafeSequCount = sdt_get_rand_number(1u,100u);	/* 生成1到100的随机SSC */

	return result;
}

/* 添加VDP尾 */ 	
SDT_RC sdsrc_add_vdp_tail(UINT8* vdpBuffer, UINT32 tailOffset, VDP_INFO_T* pVDPInfo, UINT32 sid)
{
	SDT_RC result = SDT_OK;
	UINT8* pos = NULL;

	if ((PT_IS_NULL(vdpBuffer)) || (PT_IS_NULL(pVDPInfo)))
	{
		SDT_DEBUG_MSG("[vdpBuffer || pVdpInfo ]pointer dose not exist!\n");
		result = SDT_PT_NOT_EXIST;
		return result;
	}

	pos = vdpBuffer + tailOffset;
	/*  添加reserved01	*/
	LongToChar(pVDPInfo->m_reserved01, pos);
	pos += sizeof(pVDPInfo->m_reserved01);
	/*  添加reserved02	*/
	ShortToChar(pVDPInfo->m_reserved02, pos);
	pos += sizeof(pVDPInfo->m_reserved02);

	/*  添加UDV			*/
	ShortToChar(pVDPInfo->m_UserDataVersion, pos);
	pos += sizeof(pVDPInfo->m_UserDataVersion);

	/*  添加SSC			*/
	LongToChar(pVDPInfo->m_SafeSequCount, pos);
	pos += sizeof(pVDPInfo->m_SafeSequCount);
	
	/*  计算SC			*/
	pVDPInfo->m_SafetyCode = sdt_calculate_sc(vdpBuffer, tailOffset, sid);

	/*	添加SC			*/
	LongToChar(pVDPInfo->m_SafetyCode, pos);
	pos += sizeof(pVDPInfo->m_SafetyCode);

	if (pos !=  (vdpBuffer + tailOffset + SDT_TAIL_SIZE))
	{
		SDT_DEBUG_MSG("memory access out of bounds!\n");
		memset((void*)(vdpBuffer + tailOffset), 0, SDT_TAIL_SIZE);
		result = SDT_OUT_OF_BOUND;
	}
	return result;
}



/*
*	函数名称:	sdsrc_printf_test_info
*	函数功能:	打印发送端测试信息
*	返 回 值:	无
*	参数说明:	P_SDSRC_CHANNEL	const	pSdsrcChannel	发送端通道句柄
*				TCT_SDT_UINT8	const*	vpdBuffer		vpd信息缓存指针
*				TCT_SDT_UINT32	const	vpdBufferLength vpd信息长度
*/
void sdsrc_printf_test_info(P_SDSRC_CHANNEL const pSdsrcChannel, UINT8 const* vpdBuffer, UINT32 const vpdBufferLength)
{
	fprintf(stdout, "-----------------------------------------------------------\n");
	fprintf(stdout, "当前发送端通道信息: \n");
	fprintf(stdout, "\n发送内容: %u字节\n%s \n\n", vpdBufferLength, vpdBuffer);
	fprintf(stdout, "SMI : %u\tSID : %x\n", pSdsrcChannel->m_SMI, pSdsrcChannel->m_SID);
	fprintf(stdout, "UDV : %u\tSSC : %u\n", pSdsrcChannel->m_VDPInfo.m_UserDataVersion, pSdsrcChannel->m_VDPInfo.m_SafeSequCount);
	fprintf(stdout, "SC  : %x\n", pSdsrcChannel->m_VDPInfo.m_SafetyCode);
	fprintf(stdout, "-----------------------------------------------------------\n");

}
/*	---------------------------------------测试函数定义--------------------------------------------------	*/

void test1_1(UINT8 * vdpBuffer,UINT16 vpdBufferLength)
{
	UINT8 test_errBuf[SDT_VPDMAX] = { 0 };
	if ((test_cycCount%50) == 20)
	{
		printf("HERE COMES THE TEST VDP\n");
		printf("original vdp:%s\n", vdpBuffer);
		memset(test_errBuf, 1, 2u);
		memcpy(test_errBuf + 2, (const void*)(vdpBuffer + 2), (vpdBufferLength - 2));
		memcpy((void*)vdpBuffer, test_errBuf, vpdBufferLength);
		printf("chenged vdp:%s\n", vdpBuffer);
		test_cycCount = 0;
	}
	else
	{
		test_cycCount++;
	}
}
/* 接收端功能测试函数，用于产生一组不安全的vdp序列，以验证接收端能够检测到异常并进行异常处理 */
void test1_2(UINT8* vdpBuffer, UINT16 vpdBufferLength)
{
	UINT8 test_errBuf[2] = { 0 };
	if ((test_cycCount%50) == 20)
	{
		printf("HERE COMES THE TEST VDP\n");
		printf("original udv:%d\n", (*(vdpBuffer+vpdBufferLength+6u))*256u+(*(vdpBuffer + vpdBufferLength + 7u)));
		
		memcpy(test_errBuf, (const void*)(vdpBuffer + vpdBufferLength + 6u),2u);
		test_errBuf[1]++;
		memcpy((void*)vdpBuffer + vpdBufferLength + 6u, test_errBuf, 2u);
		printf("changed udv:%d\n", (*(vdpBuffer + vpdBufferLength + 6u)) * 256u + (*(vdpBuffer + vpdBufferLength + 7u)));
		test_cycCount = 0u;
	}
	else
	{
		test_cycCount++;
	}
}
/* 接收端功能测试函数，用于产生一组不安全的vdp序列，以验证接收端能够检测到异常并进行异常处理 */
void test2_2(SDT_HANDLE sdsrcHandle)
{
	
	
	if (test_cycCount%50 == 10)
	{
		printf("HERE COMES THE TEST VDP\n");
		printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);

		g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount = g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount + 5;
		printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
		test_cycCount = 0;
	}
	else
	{
		test_cycCount++;
	}
}
/* 接收端功能测试函数，用于产生一组不安全的vdp序列，以验证接收端能够检测到异常并进行异常处理 */
void test2_1(SDT_HANDLE sdsrcHandle)
{
	if (test_cycCount == 25)
	{	
		if (test_stepCount == 0)
			test_stepCount = 5;
		switch (test_stepCount)
		{
		case 5:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);

			g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount = g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount + 3;
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 4:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);

			g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount = g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount -4;
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 3:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 2:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 1:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount = g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount +1;
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			test_cycCount = 0;
			break;
		}
		default:
			break;
		}
		
	}
	else
	{
		test_cycCount++;
	}
}
/* 接收端功能测试函数，用于产生一组不安全的vdp序列，以验证接收端能够检测到异常并进行异常处理 */
void test2_3(SDT_HANDLE sdsrcHandle)
{
	if (test_cycCount == 25)
	{
		if (test_stepCount == 0)
			test_stepCount = 5;
		switch (test_stepCount)
		{
		case 5:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);

			g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount = g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount + 1;
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 4:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 3:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 2:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount = g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount -4;
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			break;
		}
		case 1:
		{
			printf("HERE COMES THE TEST VDP\n");
			printf("original SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount = g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount + 3;
			printf("changed SSC:%d\n", g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount);
			test_stepCount--;
			test_cycCount = 0;
			break;
		}
		default:
			break;
		}

	}
	else
	{
		test_cycCount++;
	}
}


