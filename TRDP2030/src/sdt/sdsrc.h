#ifndef SDSRC_H
#define SDSRC_H

#include <stdio.h>

#include "sdt_type.h"

/*----------------------------------宏变量定义--------------------------------------------*/
#define SDSRC_CHANNEL_AMOUNT_MAX SDT_CHANNEL_AMOUNT_MAX

#define SDSRC_HANDLE_IS_INVALIDE(handle) ((0 > (handle)) || (g_sdsrc.channelNum <= (handle)))


/*----------------------------------数据类型定义------------------------------------------*/
/*发送端通道结构体定义*/
typedef struct
{
	SDT_CHANNEL_STATE		m_handleEffective;		/*	通道是否有效（1：有效，0：无效）	*/
	UINT16					m_txPeriod;             /*	源周期时间, 单位ms					*/
	UINT16					m_rxPeriod;             /*	宿周期时间, 单位ms					*/
	UINT32					m_SMI;					/*	通道SMI								*/
	SID_INFO_T				m_SIDInfo;				/*	发送端SID信息						*/                   
	UINT32					m_SID;					/*	主通道的SID值, 在通道创建时计算		*/
	VDP_INFO_T				m_VDPInfo;				/*	VDP结构体变量						*/
} SDSRC_CHANNEL, * P_SDSRC_CHANNEL;

/*发送端结构体定义*/
typedef struct
{
	SDSRC_CHANNEL	channelArray[SDSRC_CHANNEL_AMOUNT_MAX];		/* 发送端通道数组 */
	UINT32			channelCapacity;							/* 发送端通道上限 */
	UINT32			channelNum;									/* 发送端通道数量 */
}SDSRC, *P_SDSRC;


/*------------------------------------接口声明---------------------------------------------*/
/*
*	函数名称:	sdsrc_ssc_add_one
*	函数功能:	ssc值加上入参
*/
SDT_RC sdsrc_SSCvalue_add_one(SDT_HANDLE sdsrcHandle,UINT32 value);
/*
*	函数名称:	sdsrc_create_channel
*	函数功能:	创建 并 配置 发送端通道
*
*	参数说明:	SDT_HANDLE*			pSdsrcHandle	发送端句柄指针
*				SDT_CONFIG_T*		pSdsrcConfig	发送端通道配置信息
*
*	返 回 值:	SDT_OK					通道创建成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_PARAMETER	无效参数
*				SDSRC_CHANNEL_FULL		通道数量达到上限
*				SDT_INVALID_SID			SID计算错误
*
*/
SDT_RC sdsrc_create_channel(SDT_HANDLE* pSdsrcHandle, SDT_CONFIG_T* pSdsrcConfig);

/*
*	函数名称:	sdsrc_generate_VDP
*	函数功能:	发送端数据处理
*
*	参数说明:	SDT_HANDLE		sdsrcHandle		发送端通道句柄
*				UINT8*			vpdBuffer		vpd信息缓存
*				UINT32			vpdLength		vpd信息长度
*				UINT8*			vdpBuffer		vdp信息缓存
*				UINT32*			vdpLength		vdp信息长度
*
*	返 回 值:	SDT_OK					VDP生成成功
*				SDT_INVALID_HANDLE		句柄无效
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_VPD_LEN		VPD长度无效
*				SDT_OUT_OF_BOUND		访问越界
*
*/
SDT_RC sdsrc_generate_VDP(SDT_HANDLE sdsrcHandle, UINT8* vpdBuffer, UINT32 vpdLength, UINT8* vdpBuffer, UINT32* vdpLength);


/*	---------------------------------------测试函数声明--------------------------------------------------	*/
void test1_1(UINT8* vdpBuffer, UINT16 vpdBufferLength);
void test1_2(UINT8* vdpBuffer, UINT16 vpdBufferLength);
void test2_2(SDT_HANDLE sdsrcHandle);
void test2_1(SDT_HANDLE sdsrcHandle);
void test2_3(SDT_HANDLE sdsrcHandle);


#endif
