#ifndef SDSINK_H
#define SDSINK_H

#include "sdt_type.h"
#include <stdio.h>

/*----------------------------------宏变量定义----------------------------------------------*/
#define SDSINK_CHANNEL_AMOUNT_MAX SDT_CHANNEL_AMOUNT_MAX

#define SDSINK_HANDLE_IS_INVALIDE(handle) ((0 > handle) || (g_sdsink.channelNum <= handle))

#define SDSINK_COUNTER_ADD(num) (num = ((num>=COUNTER_MAX)?(COUNTER_MAX):(num+1)))

/*----------------------------------数据类型定义--------------------------------------------*/
/* 接收端统计信息结构体定义 */
typedef struct
{
    UINT32              m_newVDPCounter;                /*  新VDP计数器                         */
    UINT32              m_errorVDPCounter;              /*  错误VDP计数器                       */
    UINT32              m_switchChannelCounter;         /*  通道切换计数器                      */
    UINT32              m_wrongOrderCounter;            /*  乱序接收计数器                      */
    UINT32              m_repeatedVDPVounter;           /*  重复VDP计数器                       */
    UINT32              m_lantencyCounter;              /*  发生延迟接收周期计数器              */
    UINT32              m_wrongUDVCounter;              /*  错误UDV计数器                       */
    UINT32              m_cmcUnsafeCounter;             /*  cmc非安全周期计数器                 */
    UINT32				m_rtsUnsafeCounter;				//宿时间监视非安全周期数
    UINT32				m_VDPAmountCounter;				//解析vdp总数计数
    UINT32				m_availableVDPCounter;			//用户数据呈现给应用的vdp计数
    UINT32				m_invalidVDPCounter;			//非有效vdp计数
}SDSINK_STATISTIC_INFO_T;

/* 接收端通道结构体定义 */
typedef struct { 
    SDT_CHANNEL_STATE   m_handleEffective;				//通道激活状态
    SDT_CLOCK           m_currentProcessStartTime;      /* 当前处理过程开始时间                 */

    UINT32              m_txPeriod;                     /* 源周期时间，单位ms                   */
    UINT32              m_rxPeriod;                     /* 宿周期时间，单位ms                   */
    UINT32              m_rxSafe;                       /* 倍率, 以txPeriod周期计的超时         */
    UINT32              m_tGuard;                       /* 倍率, 以txPeriod周期计的守护时间     */
    UINT32              m_cmThr;                        /* 通道监视阈值                         */
    UINT16              m_udv;                          /* 预期用户数据版本                     */   
    UINT32              m_channelASMI;                  /* 通道A的SMI                           */
    UINT32              m_channelBSMI;                  /* 通道B的SMI                           */
    SDT_BOOL_STATE      m_redundancyOpenFlag;           /* 冗余通道开启标志                     */       
    SID_INFO_T          m_SIDInfo;                      /* SID信息                              */
    UINT32              m_channelASID;                  /* 通道A的SID值, 在通道创建时计算       */
    UINT32              m_channelBSID;                  /* 通道B的SID值, 在通道创建时计算       */
    UINT32              m_recvWindowSize;               /* 期望接收SSC的窗口大小(NSSC)          */
    UINT32              m_sendFrequencyPerHour;         /* VDP每小时发送频率                    */
    UINT32              m_wrongIncreaseStep;            /* 错误时增加步长                       */
    UINT32              m_wrongCounterUpper;            /* 错误计数上限                         */

    SDT_BOOL_STATE      m_powerOnResetFlag;             /* 上电重置标志位                       */
    VDP_INFO_T          m_VDPInfo;                      /* VDP信息                              */  
    SDT_BOOL_STATE      m_correctVDPFlag;               /* 正确VDP标志位(0: 否 1: 是)           */
    SDT_BOOL_STATE      m_repeatedVDPFlag;              /* 重复VDP标志位                        */
    SDT_BOOL_STATE      m_initialVDPFlag;               /* 初始VDP标志位                        */
    SDT_BOOL_STATE      m_newVDPFlag;                   /* 新的VDP标志位                        */
    SDT_BOOL_STATE      m_validVDPFlag;                 /* 有效VDP标志位                        */
    SDT_BOOL_STATE      m_lastVDPIsNewFlag;             /* 上一个VDP是新VDP的标志位             */
    UINT32              m_channelASC;                   /* 通道A计算得到的SC                    */
    UINT32              m_channelBSC;                   /* 通道B计算得到的SC                    */
    SDT_IDENTITY        m_sdsrcIdentity;                /* 接收到的VDP来自的源通道身份识别      */
    SDT_IDENTITY        m_lastSdsrcIdentity;            /* 上一接收到的VDP来自的源通道身份识别  */
    SDT_SAFE_STATE      m_safeComFlag;                  /* 安全通信指示位                       */ 
    SDT_SAFE_STATE      m_rtsSafeCom;                   /* 宿 时 间监视的 安全通信标志位        */
    SDT_SAFE_STATE      m_gtcSafeCom;                   /* 守护时间监视的 安全通信标志位        */
    SDT_SAFE_STATE      m_lmcSafeCom;                   /* 延    迟监视的 安全通信标志位        */
    SDT_SAFE_STATE      m_cmcSafeCom;                   /* 通    道监视的 安全通信标志位        */
    UINT32              m_lastValidVDPSSC;              /* 上一有效 VDP的SSC                    */
    UINT32              m_initialVDPSSC;                /* 初始VDP的 SSC                        */
    UINT32              m_initialSID;                   /* 初始VDP的SID                         */
    SDT_CLOCK           m_rtsTimer;                     /* 宿时间监视计时器                     */
    SDT_CLOCK           m_gtcTimer;                     /* 守护时间监视计时器                   */
    SDT_BOOL_STATE      m_gtcResetFlag;                 /* 守护时间监视重置标志位               */   
    SDT_CLOCK           m_lmcTimer;                     /* 延时监视计时器                       */ 
    SDT_BOOL_STATE      m_lmcResetFlag;                 /* 延时监视重置标志位                   */
    UINT32              m_BaseSSC;                      /* 基准SSC                              */ 
    UINT32              m_ExceptSSC;                    /* 预期SSC                              */ 
    UINT32              m_wrongVDPCounter;              /* 错误VDP计数                          */ 
   
    SDSINK_STATISTIC_INFO_T m_statisticInfo;            /* 统计信息结构体                       */
}SDSINK_CHANNEL, *P_SDSINK_CHANNEL; 


/*接收端结构体定义*/
typedef struct
{
    SDSINK_CHANNEL   channelArray[SDSINK_CHANNEL_AMOUNT_MAX];	/* 接收端端通道数组 */
    UINT32           channelCapacity;	                        /* 接收端通道上限   */
    UINT32           channelNum;		                        /* 接收端通道数量   */
}SDSINK, * P_SDSINK;


/*------------------------------------接口声明---------------------------------------------*/
/*初始化打印锁*/
void mutexinit();
/*
*	函数名称:	sdsink_create_channel
*	函数功能:	创建 并 配置 接收端通道
*
*	参数说明:	SDT_HANDLE*			pSdsinkHandle	发送端句柄指针
*				SDT_CONFIG_T*		pSdsinkConfig	发送端通道配置信息
*
*	返 回 值:	SDT_OK					创建成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_PARAMETER   相关配置参数错误
*				SDSINK_CHANNEL_FULL		当前使用通道到达上限
*				SDT_INVALID_SID			计算SID错误
*
*/
SDT_RC sdsink_create_channel(SDT_HANDLE* pSdsinkHandle, SDT_CONFIG_T * pSdsinkConfig);


/*
*   函数名称:   sdsink_parse_VDP
*   函数功能:   根据句柄对应的通道, 解析收到的PD数据
*
*   输入参数:   SDT_HANDLE		sdsinkHandle            接收端句柄
*				UINT8*			vdpBuffer				vdp数据
*				UINT16			vdpLength				vdp数据中vpd的长度
*				UINT8*			vpdBuffer				解析后的vpd数据
*				UINT32*			pVpdLength				解析后数据的长度
*
*   返 回 值:   SDT_OK					解析成功
*				SDT_INVALID_HANDLE		无效句柄
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_VPD_LEN		VPD长度不合理
*				SDT_INVALID_SC			计算SC错误
*				SDT_OUT_OF_BOUND		提取信息过程中访问越界
*
*/
SDT_RC sdsink_parse_VDP(SDT_HANDLE sdsinkHandle, UINT8 * vdpBuffer, UINT32 vdpLength, UINT8 * vpdBuffer, UINT32* pVpdLength);




#endif

