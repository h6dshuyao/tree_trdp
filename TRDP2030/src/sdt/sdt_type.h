#ifndef SDT_TYPE_H
#define SDT_TYPE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if (defined(VXWORKS))
#endif

#if ((defined(WIN32))|| defined(WIN64))
#include <time.h>
#endif




/*-----------------------------------------------宏定义--------------------------------------------*/

/* SDT_DEBUG_MSG 错误信息 */
#ifdef _DEBUG
#define SDT_DEBUG_MSG(fmt, ...) printf("SDT: %s %d %s "fmt, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
#define SDT_DEBUG_MSG(...)
#endif

/* 判断指针存在 */
#ifndef PT_IS_NULL
#define PT_IS_NULL(pointer) (NULL == (pointer))
#endif


/* SDT状态定义 */ 
#define SDT_INIT_VALUE          0u           /* 初始化值 */
#define SDT_INVALID_VALUE       0u           /* 无效值  */
#define SDT_TAIL_SIZE           16u          /* VDP报尾固定16位 */
#define SDT_VERSION             0x0002u      /* SDT版本  */
#define SDT_CRC_TABLE_SIZE      256u         /* CRC校验码表成员总个数 */
#define SDT_SID_SEED            0xffffffff   /* SID值计算种子 */
#define SDT_CHANNEL_AMOUNT_MAX  100u         /* 通道上限 */
#define SDT_UUID_SIZE           16u          /* UUID 数组尺寸 */

/* 默认值宏定义 */
#define SDT_DEFAULT_CHANNEL_B_SMI   0u       		/* 通道B的默认SMI值 */
#define SDT_DEFAULT_N_RXSAFE        3u       		/* 以tx-period周期计的超时的默认倍率 */
#define SDT_DEFAULT_N_GUARD         100u     		/* 以tx-period周期计的守护时间的默认倍率  */
#define SDT_DEFAULT_CM_THR          43u      		/* 默认参数阈值 */
#define K3                          43u      		/* 通道监视默认参数K3 */
#define K4                          36u      		/* 通道监视默认参数K4 */   
#define SDT_UINT32_MAX              0xffffffffu		/* UINT32 上限 */
#define COUNTER_MAX                 SDT_UINT32_MAX	/*使用UINT32作为自增计数值的计数上限*/

/* 参数范围宏定义 */
#define SDT_VPDMIN          0u			/* 最小用户数据长度 0810mod:改成1，需要讨论 */
#define SDT_VPDMAX          984u		/* 最大用户数据长度 */
#define SDT_UDV_MIN         0x0100u		/* UDV版本号最小值  */  
#define SDT_UDV_MAX	        0xFFFFu		/* UDV版本号最大值 */  
#define SDT_PERIOD_MIN      20u			/* 周期时间最小为: 20 ms */
#define SDT_PERIOD_MAX      10*1000u	/* 周期时间最大为: 10 * 1000 ms */
#define SDT_N_SAFE_MIN      3u			/* 安全周期倍率最小值 */ 
#define SDT_N_SAFE_MAX      10u			/* 安全周期倍率最大值 */    
#define SDT_N_GUARD_MIN     2u			/* 守护周期倍率最小值 */  
#define SDT_N_GUARD_MAX     1000u		/* 守护周期倍率最大值 */ 

/*-------------------------------------------数据类型定义--------------------------------------------*/

#if ((defined(WIN32))|| (defined(WIN64)))
typedef unsigned char       UINT8;
typedef unsigned short      UINT16;
typedef unsigned int        UINT32;         
typedef unsigned long long  UINT64;
typedef signed char         INT8;
typedef signed short        INT16;
typedef signed int          INT32;
typedef signed long long    INT64;
typedef unsigned char       BOOL8;
typedef unsigned char       BITSET8;
typedef unsigned char       ANTIVALENT8;
typedef char                CHAR8;
typedef short               UTF16;
typedef float               REAL32;
typedef double              REAL64;
typedef UINT32              SDT_HANDLE;
typedef clock_t             SDT_CLOCK;
#elif (defined(VXWORKS))
typedef unsigned char       UINT8;         
typedef unsigned short      UINT16;       
typedef unsigned int        UINT32;        
typedef unsigned long long  UINT64;         
typedef signed char         INT8;
typedef signed short        INT16;
typedef signed int          INT32;         
typedef signed long long    INT64;          
typedef unsigned char       BOOL8;
typedef unsigned char       BITSET8;
typedef unsigned char       ANTIVALENT8;
typedef char                CHAR8;         
typedef short               UTF16;
typedef float               REAL32;
typedef double              REAL64;         
typedef clock_t             SDT_CLOCK;     
typedef UINT32              SDT_HANDLE;    
#endif

/* SDT 接口返回值定义 */ 
typedef enum 
{
    SDT_OK= 0x0000,                 /* 成功状态         */
    SDT_ERROR,                      /* 失败状态         */
    SDT_PT_NOT_EXIST,               /* 指针不存在       */
    SDT_OUT_OF_BOUND,               /* 越界             */ 
    SDT_INVALID_VPD_LEN,            /* vpd长度无效      */
    SDT_INVALID_PARAMETER,          /* 参数无效         */
    SDT_INVALID_HANDLE,             /* 句柄无效         */
    SDT_INVALID_SID,                /* SID无效          */  
    SDT_INVALID_SC,                 /* SC无效           */
    SDT_INVALID_VPD,                /* VPD数据无效      */
    SDSRC_CHANNEL_FULL,             /* 发送端通道满     */
    SDSINK_CHANNEL_FULL,            /* 接收端通道满     */
} SDT_RC;

/* 布尔状态 二值定义 */
typedef enum
{
    SDT_FALSE   = 0xaa,  
    SDT_TRUE    = 0x55, 
} SDT_BOOL_STATE;

/* SDT 通道安全标志 */ 
typedef enum
{
    SDT_UNSAFE  = SDT_FALSE,        /* SDT通道不安全 */
    SDT_SAFE    = SDT_TRUE,         /* SDT通道安全   */
} SDT_SAFE_STATE;

/* SDT 通道有效状态 */ 
typedef enum
{
    SDT_UNEFFECTIVE = SDT_FALSE,    /* SDT通道无效 */
    SDT_EFFECTIVE   = SDT_TRUE,       /* SDT通道有效 */
} SDT_CHANNEL_STATE;

/* SDT 通道身份识别*/ 
typedef enum
{
    STA_SDSINK_CHANNEL=0,           /* 接收端(默认) */
    STA_SDSRC_CHANNEL_A,            /* 发送端通道A */
    STA_SDSRC_CHANNEL_B             /* 发送端通道B */
} SDT_IDENTITY;

/* SDT配置参数结构体 */
typedef struct 
{
	UINT32  sdtHandle;				/* 保存用配置建立通道后返回的通道句柄 */
	UINT32  comId;					/* 保存sdt配置对应的comid */
	
	UINT32  datasetId;				/* 保存sdt配置对应的datasetId 暂时没用 */
	UINT32  comParId;				/* 保存sdt配置对应的comParId 没用 */
	UINT32  ipId;					/* 保存sdt配置对应的ipId 暂时没用 */
	UINT32  datasetSize;			/* 保存sdt配置对应的datasetSize 暂时没用 */
	UINT32	offset;					/* 保存sdt配置对应的offset 暂时没用 */
	
    UINT32  smi1;                   /* 通道Asmi, 编组级此信息唯一 */
    UINT32  smi2;                   /* 通道Bsmi, 编组级此信息唯一 */
    UINT16  udv;                    /* 用户数据版本 */
    UINT16  rxPeriod;               /* 宿周期时间, 单位ms */
    UINT16  txPeriod;               /* 源周期时间, 单位ms */    
    UINT8   nRxSafe;                /* 倍率, 以tx-period周期计的超时 */
    UINT16  nGuard;                 /* 倍率, 以tx-period周期计的守护时间 */
    UINT32  cmThr;                  /* 通道监视阈值 */ 
    SDT_IDENTITY channelIdentity;   /* 通道身份 */
} SDT_CONFIG_T, *P_SDT_CONFIG_T;

/* VDP结构体定义     */ 
typedef struct
{
    UINT32 m_reserved01;            /* 保留位01 */
    UINT16 m_reserved02;            /* 保留位02 */
    UINT16 m_UserDataVersion;       /* 重要过程数据版本 */
    UINT32 m_SafeSequCount;         /* 安全序列计数器  */
    UINT32 m_SafetyCode;            /* 安全验证码 */
} VDP_INFO_T; 

/*  SID结构体定义    */ 
typedef struct
{
    UINT16 reserved01;              /* 保留位01            */
    UINT16 SDTProtVers;             /* SDTv2协议版本信息   */
    UINT8  cstUUID[SDT_UUID_SIZE];  /* 唯一编组标识符      */
    UINT32 safeTopoCount;           /* 安全拓扑计数器      */
    UINT32 reserved02;              /* 保留位02            */
} SID_INFO_T;


/* 初始化定义CRC校验码表 */
static const UINT32 g_tct_sdt_crc32_table[SDT_CRC_TABLE_SIZE] =
{ 0x00000000, 0xF4ACFB13, 0x1DF50D35, 0xE959F626,
 0x3BEA1A6A, 0xCF46E179, 0x261F175F, 0xD2B3EC4C,
 0x77D434D4, 0x8378CFC7, 0x6A2139E1, 0x9E8DC2F2,
 0x4C3E2EBE, 0xB892D5AD, 0x51CB238B, 0xA567D898,
 0xEFA869A8, 0x1B0492BB, 0xF25D649D, 0x06F19F8E,
 0xD44273C2, 0x20EE88D1, 0xC9B77EF7, 0x3D1B85E4,
 0x987C5D7C, 0x6CD0A66F, 0x85895049, 0x7125AB5A,
 0xA3964716, 0x573ABC05, 0xBE634A23, 0x4ACFB130,
 0x2BFC2843, 0xDF50D350, 0x36092576, 0xC2A5DE65,
 0x10163229, 0xE4BAC93A, 0x0DE33F1C, 0xF94FC40F,
 0x5C281C97, 0xA884E784, 0x41DD11A2, 0xB571EAB1,
 0x67C206FD, 0x936EFDEE, 0x7A370BC8, 0x8E9BF0DB,
 0xC45441EB, 0x30F8BAF8, 0xD9A14CDE, 0x2D0DB7CD,
 0xFFBE5B81, 0x0B12A092, 0xE24B56B4, 0x16E7ADA7,
 0xB380753F, 0x472C8E2C, 0xAE75780A, 0x5AD98319,
 0x886A6F55, 0x7CC69446, 0x959F6260, 0x61339973,
 0x57F85086, 0xA354AB95, 0x4A0D5DB3, 0xBEA1A6A0,
 0x6C124AEC, 0x98BEB1FF, 0x71E747D9, 0x854BBCCA,
 0x202C6452, 0xD4809F41, 0x3DD96967, 0xC9759274,
 0x1BC67E38, 0xEF6A852B, 0x0633730D, 0xF29F881E,
 0xB850392E, 0x4CFCC23D, 0xA5A5341B, 0x5109CF08,
 0x83BA2344, 0x7716D857, 0x9E4F2E71, 0x6AE3D562,
 0xCF840DFA, 0x3B28F6E9, 0xD27100CF, 0x26DDFBDC,
 0xF46E1790, 0x00C2EC83, 0xE99B1AA5, 0x1D37E1B6,
 0x7C0478C5, 0x88A883D6, 0x61F175F0, 0x955D8EE3,
 0x47EE62AF, 0xB34299BC, 0x5A1B6F9A, 0xAEB79489,
 0x0BD04C11, 0xFF7CB702, 0x16254124, 0xE289BA37,
 0x303A567B, 0xC496AD68, 0x2DCF5B4E, 0xD963A05D,
 0x93AC116D, 0x6700EA7E, 0x8E591C58, 0x7AF5E74B,
 0xA8460B07, 0x5CEAF014, 0xB5B30632, 0x411FFD21,
 0xE47825B9, 0x10D4DEAA, 0xF98D288C, 0x0D21D39F,
 0xDF923FD3, 0x2B3EC4C0, 0xC26732E6, 0x36CBC9F5,
 0xAFF0A10C, 0x5B5C5A1F, 0xB205AC39, 0x46A9572A,
 0x941ABB66, 0x60B64075, 0x89EFB653, 0x7D434D40,
 0xD82495D8, 0x2C886ECB, 0xC5D198ED, 0x317D63FE,
 0xE3CE8FB2, 0x176274A1, 0xFE3B8287, 0x0A977994,
 0x4058C8A4, 0xB4F433B7, 0x5DADC591, 0xA9013E82,
 0x7BB2D2CE, 0x8F1E29DD, 0x6647DFFB, 0x92EB24E8,
 0x378CFC70, 0xC3200763, 0x2A79F145, 0xDED50A56,
 0x0C66E61A, 0xF8CA1D09, 0x1193EB2F, 0xE53F103C,
 0x840C894F, 0x70A0725C, 0x99F9847A, 0x6D557F69,
 0xBFE69325, 0x4B4A6836, 0xA2139E10, 0x56BF6503,
 0xF3D8BD9B, 0x07744688, 0xEE2DB0AE, 0x1A814BBD,
 0xC832A7F1, 0x3C9E5CE2, 0xD5C7AAC4, 0x216B51D7,
 0x6BA4E0E7, 0x9F081BF4, 0x7651EDD2, 0x82FD16C1,
 0x504EFA8D, 0xA4E2019E, 0x4DBBF7B8, 0xB9170CAB,
 0x1C70D433, 0xE8DC2F20, 0x0185D906, 0xF5292215,
 0x279ACE59, 0xD336354A, 0x3A6FC36C, 0xCEC3387F,
 0xF808F18A, 0x0CA40A99, 0xE5FDFCBF, 0x115107AC,
 0xC3E2EBE0, 0x374E10F3, 0xDE17E6D5, 0x2ABB1DC6,
 0x8FDCC55E, 0x7B703E4D, 0x9229C86B, 0x66853378,
 0xB436DF34, 0x409A2427, 0xA9C3D201, 0x5D6F2912,
 0x17A09822, 0xE30C6331, 0x0A559517, 0xFEF96E04,
 0x2C4A8248, 0xD8E6795B, 0x31BF8F7D, 0xC513746E,
 0x6074ACF6, 0x94D857E5, 0x7D81A1C3, 0x892D5AD0,
 0x5B9EB69C, 0xAF324D8F, 0x466BBBA9, 0xB2C740BA,
 0xD3F4D9C9, 0x275822DA, 0xCE01D4FC, 0x3AAD2FEF,
 0xE81EC3A3, 0x1CB238B0, 0xF5EBCE96, 0x01473585,
 0xA420ED1D, 0x508C160E, 0xB9D5E028, 0x4D791B3B,
 0x9FCAF777, 0x6B660C64, 0x823FFA42, 0x76930151,
 0x3C5CB061, 0xC8F04B72, 0x21A9BD54, 0xD5054647,
 0x07B6AA0B, 0xF31A5118, 0x1A43A73E, 0xEEEF5C2D,
 0x4B8884B5, 0xBF247FA6, 0x567D8980, 0xA2D17293,
 0x70629EDF, 0x84CE65CC, 0x6D9793EA, 0x993B68F9
};

#endif
