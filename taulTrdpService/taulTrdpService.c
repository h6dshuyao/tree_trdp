/**********************************************************************************************************************/
/**
 * @file		taulApp.c
 *
 * @brief		Demo TAUL application for TRDP
 *
 * @details	TRDP Ladder Topology Support TAUL Application
 *
 * @note		Project: TCNOpen TRDP prototype stack
 *
 * @author		Kazumasa Aiba, Toshiba Corporation
 *
 * @remarks This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Toshiba Corporation, Japan, 2013. All rights reserved.
 *
 * $Id: taulApp_PD_sample.c 2054 2019-08-28 11:03:37Z bloehr $
 *
 */

#ifdef TRDP_OPTION_LADDER
/***********************************************************************************************************************
 * INCLUDES
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>

#include "trdp_if_light.h"
#include "vos_thread.h"
#include "trdp_private.h"

#ifdef __linux__
	#include <uuid/uuid.h>
#endif

#include "taulTrdpService.h"
#include "tau_ldLadder_config.h"





#define TRDP_SVC_VER	"v1.0.0.4"

/*******************************************************************************
 * DEFINES
 */
#define SOURCE_URI "user@host"
#define DEST_URI "user@host"
#define TDRP_VAR_SIZE 0
#define DDS_MESSAGE_HEADLENTH 6
#define DDS_DATALENTH_MAX 1432

#define SHARE_MEMORY_LIST_OFFSET  0x3F700

#define APP_TO_REDUN_DDS_DATALENTH 27
#pragma pack(1)

/*******************************************************************************
 * TYPEDEFS
 */
typedef struct
{
    UINT32                  comId;                              /* handle from related subscribe */
    UINT32                  offset;                                
    UINT16     				datasize;                 /* dataset (size, buffer) */
}TRDP_APP_INDEX;

typedef struct
{
    UINT32                  comId;                              /* handle from related subscribe */
    UINT32                  datasize;             
    PUBLISH_TELEGRAM_T*		pPublishTelegram;/* pointer to corresponding PublishTelegram */    
}PUBLISHER_ELE;

/******************************************************************************
 *   Locals
 */
void sharememReceiveAppData();
STATUS ddsSendAppData(UINT32 comid,UINT32 datalenth,UINT8* ptrdpData,UINT32 offset);
TAUL_APP_ERR_TYPE myCreateDataset (
		TRDP_DATASET_T			*pDatasetDesc,
		UINT8					*pDstEnd,
		UINT8					*Buffer);
void SetRedundant(UINT32 stopTrdpSendOrNot);
UINT32 parse_DDSredundant_message(UINT8* data,UINT32 datalenth);
void ddsReadSendRedunData();
int createDDSThread(VOS_THREAD_FUNC_T pfunction,CHAR8 * ThreadName,void * pArguments,UINT32 interval);
void testLock();

/******************************************************************************
 *   Globals
 */

#ifdef XML_CONFIG_ENABLE
const CHAR8 appXmlConfigFileName[FILE_NAME_MAX_SIZE] = "/mmc0a/xmlconfig.xml";      /* XML Config File Name */
#endif /* ifdef XML_CONFIG_ENABLE */



/* Thread Name */
CHAR8 publisherThreadName[] ="PublisherThread";							/* Thread name is Publisher Application Thread. */
CHAR8 subscriberThreadName[] ="SubscriberThread";						/* Thread name is Subscriber Application Thread. */

UINT32 pubTelegramNuber=0;
UINT32 subTelegramNuber=0;
/* Thread Stack Size */
UINT32    threadStackSize   = 256 * 1024;


/* DDS APP Data List */
PUBLISHER_ELE ddsAppDataList[APPLICATION_THREAD_LIST_MAX]={{0}};
TRDP_APP_SESSION_T appHandleList[2];

/* Create Role Application Thread */
TRDP_APP_INDEX comidOffsetList[APPLICATION_THREAD_LIST_MAX]={0};


/******************************************************************************
 *   Function
 */
/**********************************************************************************************************************/
/** callback routine for TRDP logging/error output
 *
 *  @param[in]		pRefCon		user supplied context pointer
 *  @param[in]		category		Log category (Error, Warning, Info etc.)
 *  @param[in]		pTime			pointer to NULL-terminated string of time stamp
 *  @param[in]		pFile			pointer to NULL-terminated string of source module
 *  @param[in]		LineNumber		line
 *  @param[in]		pMsgStr       pointer to NULL-terminated string
 *  @retval         none
 */
void dbgOut (
    void        *pRefCon,
    TRDP_LOG_T  category,
    const CHAR8 *pTime,
    const CHAR8 *pFile,
    UINT16      LineNumber,
    const CHAR8 *pMsgStr)
{
    const char *catStr[] = {"**Error:", "Warning:", "   Info:", "  Debug:"};
    BOOL8 logPrintOnFlag = FALSE;							/* FALSE is not print */
    FILE *fpOutputLog = NULL;								/* pointer to Output Log */
    const TRDP_FILE_NAME_T logFileNameNothing = {0};		/* Log File Name Nothing */

    /* Check Output Log Enable from Debug Config */
    switch(category)
    {
    	case VOS_LOG_ERROR:
			/* Debug Config Option : ERROR or WARNING or INFO or DEBUG */
			if (((debugConfigTAUL.option & TRDP_DBG_ERR) == TRDP_DBG_ERR)
				|| ((debugConfigTAUL.option & TRDP_DBG_WARN) == TRDP_DBG_WARN)
				|| ((debugConfigTAUL.option & TRDP_DBG_INFO) == TRDP_DBG_INFO)
				|| ((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG))
			{
				logPrintOnFlag = TRUE;
			}
		break;
    	case VOS_LOG_WARNING:
			/* Debug Config Option : WARNING or INFO or DEBUG */
    		if (((debugConfigTAUL.option & TRDP_DBG_WARN) == TRDP_DBG_WARN)
    			|| ((debugConfigTAUL.option & TRDP_DBG_INFO) == TRDP_DBG_INFO)
				|| ((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG))
			{
				logPrintOnFlag = TRUE;
			}
    	break;
    	case VOS_LOG_INFO:
			/* Debug Config Option : INFO or DBUG */
    		if (((debugConfigTAUL.option & TRDP_DBG_INFO) == TRDP_DBG_INFO)
    			|| ((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG))
			{
				logPrintOnFlag = TRUE;
			}
    	break;
    	case VOS_LOG_DBG:
			/* Debug Config Option : DEBUG */
			if((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG)
			{
				logPrintOnFlag = TRUE;
			}
		break;
    	default:
    	break;
    }

    /* Check log Print */
    if (logPrintOnFlag == TRUE)
    {   
    	/* Check output place of Log */
    	if (memcmp(debugConfigTAUL.fileName, logFileNameNothing,sizeof(TRDP_FILE_NAME_T)) != 0)
    	{
    	    /* Open file in append mode */
    		fpOutputLog = fopen(debugConfigTAUL.fileName, "a");
    	    if (fpOutputLog == NULL)
    	    {
    	    	vos_printLog(VOS_LOG_ERROR, "dbgOut() Log File Open Err\n");
    			return;
    	    }
    	}
    	else
    	{
    		/* Set Output place to Display */
    		fpOutputLog = stdout;
    	}
    	/* Output Log Information */
		/*  Log Date and Time */
		if (debugConfigTAUL.option & TRDP_DBG_TIME)
			fprintf(fpOutputLog, "%s ", pTime);
		/*  Log category */
		if (debugConfigTAUL.option & TRDP_DBG_CAT)
			fprintf(fpOutputLog, "%s ", catStr[category]);
		/*  Log Source File Name and Line */
		if (debugConfigTAUL.option & TRDP_DBG_LOC)
			fprintf(fpOutputLog, "%s:%d ", pFile, LineNumber);
		/*  Log message */
		fprintf(fpOutputLog, "%s", pMsgStr);

		if (fpOutputLog != stdout)
		{
			/* Close Log File */
			fclose(fpOutputLog);
		}
    }
}









/**********************************************************************************************************************/
/** Application Thread Common Function
 */

#if 0
/**********************************************************************************************************************/
/** Display MD Replier Receive Count
 *
 *  @param[in]      pHeadCommandValue	pointer to head of queue
 *  @param[in]      commandValueId		COMMAND_VALUE ID
 *
 *  @retval         TAUL_APP_NO_ERR					no error
 *  @retval         MD_PARAM_ERR					parameter	error
 *
 */
TAU_APP_ERR_TYPE printReplierResult (
		COMMAND_VALUE	*pHeadCommandValue,
		UINT32 commandValueId)
{
	COMMAND_VALUE *iterCommandValue;
	UINT16 commnadValueNumber = 1;
	char strIp[16] = {0};

    if (pHeadCommandValue == NULL)
    {
        return MD_APP_PARAM_ERR;
    }

    for (iterCommandValue = pHeadCommandValue;
    	  iterCommandValue != NULL;
    	  iterCommandValue = iterCommandValue->pNextCommandValue)
    {
		/* Check Valid Replier */
		if ((iterCommandValue->mdCallerReplierType == REPLIER) && (pHeadCommandValue->mdAddListenerComId != 0))
		{
			/* Check ALL commanValue or select commandValue */
			if( (commandValueId == 0)
			|| ((commandValueId != 0) && (commandValueId == iterCommandValue->commandValueId)))
			{
				/*  Dump CommandValue */
				printf("Replier No.%u\n", commnadValueNumber);
				printf("-c,	Transport Type (UDP:0, TCP:1): %u\n", iterCommandValue->mdTransportType);
				printf("-d,	Replier Reply Message Type (Mp:0, Mq:1): %u\n", iterCommandValue->mdMessageKind);
				miscIpToString(iterCommandValue->mdDestinationAddress, strIp);
				printf("-g,	Replier MD Receive Destination IP Address: %s\n", strIp);
	//			printf("-i,	Dump Type (DumpOn:1, DumpOff:0, 0bit:Operation Log, 1bit:Send Log, 2bit:Receive Log): %u\n", iterCommandValue->mdDump);
				printf("-k,	Replier MD Request Receive Cycle Number: %u\n", iterCommandValue->mdCycleNumber);
	//			printf("-l,	Log Type (LogFileOn:1, LogFileOff:0, 0bit:Operation Log, 1bit:Send Log, 2bit:Receive Log): %u\n", iterCommandValue->mdLog);
				printf("-n,	Topology TYpe (Ladder:1, not Lader:0): %u\n", iterCommandValue->mdLadderTopologyFlag);
				printf("-N,	Confirm TImeout: micro sec: %u\n", iterCommandValue->mdTimeoutConfirm);
				printf("-o,	Replier MD Reply Error Type(1-6): %u\n", iterCommandValue->mdReplyErr);
				printf("-p,	Marshalling Type (Marshall:1, not Marshall:0): %u\n", iterCommandValue->mdMarshallingFlag);
				printf("-q,	Replier Add Listener ComId: %u\n", iterCommandValue->mdReplierNumber);
	//			printf("-r,	Reply TImeout: %u micro sec\n", iterCommandValue->mdTimeoutReply);
//				printf("Replier Receive MD Count: %u\n", iterCommandValue->replierMdReceiveCounter);
				printf("Replier Receive MD Count: %u\n", iterCommandValue->replierMdRequestReceiveCounter + iterCommandValue->replierMdConfrimReceiveCounter);
				printf("Replier Receive MD Request(Mn,Mr) Count: %u\n", iterCommandValue->replierMdRequestReceiveCounter);
				printf("Replier Receive MD Confirm(Mc) Count: %u\n", iterCommandValue->replierMdConfrimReceiveCounter);
				printf("Replier Receive MD Success Count: %u\n", iterCommandValue->replierMdReceiveSuccessCounter);
				printf("Replier Receive MD Failure Count: %u\n", iterCommandValue->replierMdReceiveFailureCounter);
				printf("Replier Retry Count: %u\n", iterCommandValue->replierMdRetryCounter);
				printf("Replier Send MD Count: %u\n", iterCommandValue->replierMdSendCounter);
				printf("Replier Send MD Success Count: %u\n", iterCommandValue->replierMdSendSuccessCounter);
				printf("Replier Send MD Failure Count: %u\n", iterCommandValue->replierMdSendFailureCounter);
				commnadValueNumber++;
			}
		}
    }

    if (commnadValueNumber == 1 )
    {
    	printf("Valid Replier MD Command isn't Set up\n");
    }

    return TAUL_APP_NO_ERR;
}
#endif




/**********************************************************************************************************************/
/** TAUL Application initialize
 *
 *  @retval         0		no error
 *  @retval         1		some error
 */
TAUL_APP_ERR_TYPE initTaulApp (
		void)
{
	TRDP_ERR_T								err = TRDP_NO_ERR;
	PUBLISH_TELEGRAM_T					*iterPublishTelegram = NULL;
	extern PUBLISH_TELEGRAM_T			*pHeadPublishTelegram;
	SUBSCRIBE_TELEGRAM_T					*iterSubscribeTelegram = NULL;
	extern SUBSCRIBE_TELEGRAM_T			*pHeadSubscribeTelegram;
	extern UINT8        				*pTrafficStoreAddr;
	UINT32									publisherApplicationId = 0;					/* Publisher Thread Application Id */
	UINT32									subscriberApplicationId = 0;				/* Subscriber Thread Application Id */
	UINT32									taulApplicationThreadId = 0;				/* TAUL Application Thread Id */

	/* Using Receive Subnet in order to Write PD in Traffic Store  */
	TAU_LD_CONFIG_T						ladderConfig = {0};
	UINT32									index = 0;							/* Loop Counter */
	
	/* For Get IP Address */
	UINT32 getNoOfIfaces = NUM_ED_INTERFACES;
	VOS_IF_REC_T ifAddressTable[NUM_ED_INTERFACES];
	TRDP_IP_ADDR_T ownIpAddress = 0;
	
#ifdef __linux
	CHAR8 SUBNETWORK_ID1_IF_NAME[] = "eth0";
//#elif defined(__APPLE__)
#else
	CHAR8 SUBNETWORK_ID1_IF_NAME[] = "temac0";
#endif
	int i = 0;

#ifdef XML_CONFIG_ENABLE
	/* Set XML Config */
	strncpy(xmlConfigFileName, appXmlConfigFileName, sizeof(xmlConfigFileName));
#endif /* ifdef XML_CONFIG_ENABLE */

	/* Get I/F address */
	if (vos_getInterfaces(&getNoOfIfaces, ifAddressTable) != VOS_NO_ERR)
	{
		printf("main() failed. vos_getInterfaces() error.\n");
		return TAUL_INIT_ERR;
	}
	

	/* Get All I/F List */
	for (index = 0; index < getNoOfIfaces; index++)
	{
		if (strncmp(ifAddressTable[index].name, SUBNETWORK_ID1_IF_NAME, sizeof(SUBNETWORK_ID1_IF_NAME)) == 0)
		{
				/* Get Sub-net Id1 Address */
			ownIpAddress = (TRDP_IP_ADDR_T)(ifAddressTable[index].ipAddr);
			break;
		}
	}

	/* Set Ladder Config */
	ladderConfig.ownIpAddr = ownIpAddress;
	
	/* Initialize TAUL */
	err = tau_ldInit(dbgOut, &ladderConfig);
	
	if (err != TRDP_NO_ERR)
	{
		printf("TRDP Ladder Support Initialize failed. tau_ldInit() error: %d \n", err);
		return TAUL_INIT_ERR;
	}
	
	/* Display TAUL Application Version */
	vos_printLog(VOS_LOG_INFO,
					"TAUL Application Version %s: TRDP Setting successfully\n",
					TAUL_APP_VERSION);

	/* Set using Sub-Network :AUTO*/
    err = tau_ldSetNetworkContext(SUBNET_AUTO);
    if (err != TRDP_NO_ERR)
    {
    	vos_printLog(VOS_LOG_ERROR, "Set Writing Traffic Store Sub-network error\n");
        return TAUL_APP_ERR;
    }

    if (err != TRDP_NO_ERR)
    {
    	vos_printLog(VOS_LOG_ERROR, "Get Writing Traffic Store Sub-network error\n");
        return TAUL_APP_ERR;
    }

	/* Read Subscribe Telegram */
	TRDP_APP_SESSION_T firstSubAppHandle=NULL;
	for (iterSubscribeTelegram = pHeadSubscribeTelegram;
			iterSubscribeTelegram != NULL;
			iterSubscribeTelegram = iterSubscribeTelegram->pNextSubscribeTelegram)
	{
		if(0==subscriberApplicationId)
			firstSubAppHandle=iterSubscribeTelegram->appHandle;
		else
		{
			if(firstSubAppHandle!=iterSubscribeTelegram->appHandle)
				continue;
		}

		/* Set comidOffsetList */
		comidOffsetList[taulApplicationThreadId].comId=htonl(iterSubscribeTelegram->comId);
		comidOffsetList[taulApplicationThreadId].offset=htonl(iterSubscribeTelegram->pPdParameter->offset);
		comidOffsetList[taulApplicationThreadId].datasize=htons((UINT16)iterSubscribeTelegram->dataset.size);

		taulApplicationThreadId++;
		subscriberApplicationId++;
	}
	subTelegramNuber=subscriberApplicationId;
	printf("read subsriber finished,sub telegram number:%d\n",subTelegramNuber);
	
	/* Read Publish Telegram */
    TRDP_APP_SESSION_T firstPubAppHandle=NULL;
	for (iterPublishTelegram = pHeadPublishTelegram;
			iterPublishTelegram != NULL;
			iterPublishTelegram = iterPublishTelegram->pNextPublishTelegram)
	{
		if(0==publisherApplicationId)
		{
			firstPubAppHandle=iterPublishTelegram->appHandle;
			appHandleList[0]=iterPublishTelegram->appHandle;
		}
		else
		{
			if(firstPubAppHandle!=iterPublishTelegram->appHandle)
			{
				appHandleList[1]=iterPublishTelegram->appHandle;
				continue;
			}
		}
		
		/* Set dds App Data List */
		ddsAppDataList[publisherApplicationId].comId=iterPublishTelegram->comId;
		ddsAppDataList[publisherApplicationId].datasize=iterPublishTelegram->dataset.size;
		ddsAppDataList[publisherApplicationId].pPublishTelegram= iterPublishTelegram;
		
		publisherApplicationId++;
		
		/* Set comidOffsetList */
		comidOffsetList[taulApplicationThreadId].comId=htonl(iterPublishTelegram->comId);
		comidOffsetList[taulApplicationThreadId].offset=htonl(iterPublishTelegram->pPdParameter->offset);
		comidOffsetList[taulApplicationThreadId].datasize=htons((UINT16)iterPublishTelegram->dataset.size);

		taulApplicationThreadId++;
    }
	
	pubTelegramNuber=publisherApplicationId;
	printf("read publisher finished,pub telegram number:%d\n",pubTelegramNuber);
	
	UINT32 tempp=htonl(pubTelegramNuber);
	UINT32 temps=htonl(subTelegramNuber);
	
	UINT32 listLenth=sizeof(TRDP_APP_INDEX)*taulApplicationThreadId;
	printf("listlenth:%d\n",listLenth);
	
	if(listLenth>TRAFFIC_STORE_MUTEX_VALUE_AREA-SHARE_MEMORY_LIST_OFFSET-8)
	{
		printf("not enough space for comid-offset list,requare %d bytes,prepare %d bytes,tail data loss\n",listLenth,TRAFFIC_STORE_MUTEX_VALUE_AREA-SHARE_MEMORY_LIST_OFFSET-8);
		listLenth=TRAFFIC_STORE_MUTEX_VALUE_AREA-SHARE_MEMORY_LIST_OFFSET-8;
	}
	/* Set comid-offset List in sharemem*/
    tau_ldLockTrafficStore();
    memcpy((void *)(pTrafficStoreAddr + SHARE_MEMORY_LIST_OFFSET), (const void *)&temps, 4);
    memcpy((void *)(pTrafficStoreAddr + SHARE_MEMORY_LIST_OFFSET+4),(const void *)&tempp, 4);
    memcpy((void *)(pTrafficStoreAddr + SHARE_MEMORY_LIST_OFFSET+8), (const void *)comidOffsetList, listLenth);
    tau_ldUnlockTrafficStore();
    
    TRDP_APP_INDEX* checkList=(TRDP_APP_INDEX*)vos_memAlloc(taulApplicationThreadId*sizeof(TRDP_APP_INDEX));
    memset((void *)checkList,0,taulApplicationThreadId*sizeof(TRDP_APP_INDEX));
    TRDP_ERR_T lock_rc=TRDP_NO_ERR;
    lock_rc=tau_ldLockTrafficStore();
    if(TRDP_NO_ERR==lock_rc)
    {
    	memcpy((void *)checkList, (UINT8 *)(pTrafficStoreAddr + SHARE_MEMORY_LIST_OFFSET+8), listLenth);
    	
    	lock_rc=tau_ldUnlockTrafficStore();
    	if(TRDP_NO_ERR!=lock_rc)
    	{
    		printf("tau_ldUnlockTrafficStore failed\n");
    		return TAUL_APP_MUTEX_ERR;
    	}
    }
    else
    {
    	printf("LockTrafficStore failed\n");
    	return TAUL_APP_MUTEX_ERR;
    }
    
    printf("set comid-offset-datasize finished:\n");
    for(int qq=0;qq<taulApplicationThreadId;qq++)
    {
    	printf("comid:%08d	offset:%08d	datasize:%04d\n",htonl(checkList[qq].comId),htonl(checkList[qq].offset),htons(checkList[qq].datasize));
    }
    printf("\n");
	return TAUL_APP_NO_ERR;
}

/**********************************************************************************************************************/
/** main entry
 *
 *  @retval         0		no error
 *  @retval         1		some error
 */


int main (INT32 argc, CHAR8 *argv[])
{
	TAUL_APP_ERR_TYPE								err = TAUL_APP_NO_ERR;
	
	//vos_mutexCreate(&pmyPrintMutex);

	printf("########## %s ########## \r\n",TRDP_SVC_VER);
	
	/* Taul Application Init */
	err = initTaulApp();
	
	if (err != TAUL_APP_NO_ERR)
	{
		printf("initTaulApp() failed,rt=0x%x\n",err);
		return 0;
	}
	/*for(int j=subTelegramNuber;j<subTelegramNuber+pubTelegramNuber;j++)
	{
		UINT8 checkBuffer[1223]={0};
		
		tau_ldLockTrafficStore();
		memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset),2,htons(comidOffsetList[j].datasize)-16);
		tau_ldUnlockTrafficStore();

	}*/
	
	vos_threadDelay(3000000);
	//CHAR8 ddsReaderThreadName[] ="ddsReaderThread";

	//createDDSThread((VOS_THREAD_FUNC_T)testLock,ddsReaderThreadName,NULL,0);
	int counter=0;
	while(1)
	{
		taskDelay(5000);
		/*for(int j=0;j<subTelegramNuber+pubTelegramNuber;j++)
		{
			UINT8 checkBuffer[1223]={0};
			
			tau_ldLockTrafficStore();
			memcpy(checkBuffer,pTrafficStoreAddr+htonl(comidOffsetList[j].offset),htons(comidOffsetList[j].datasize));
			tau_ldUnlockTrafficStore();
			
			printf("buffer[%d;%d;%d]:",htonl(comidOffsetList[j].comId),htonl(comidOffsetList[j].offset),htons(comidOffsetList[j].datasize));
			for(int q=0;q<htons(comidOffsetList[j].datasize);q++)
				printf("%02x ",checkBuffer[q]);
			printf("\n");
		}
		
		for(int j=subTelegramNuber;j<subTelegramNuber+pubTelegramNuber;j++)
		{		
			tau_ldLockTrafficStore();
			memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset),counter,htons(comidOffsetList[j].datasize)-16);
			memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset)+10,0,1);
			memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset)+11,15,1);
			tau_ldUnlockTrafficStore();

		}
		counter++;*/
	}
}

void testLock()
{
	while(1)
	{
		tau_ldLockTrafficStore();
		(void)vos_threadDelay(5000000);
		tau_ldUnlockTrafficStore();
		(void)vos_threadDelay(5000000);
	}
}

int createDDSThread(VOS_THREAD_FUNC_T pfunction,CHAR8 * ThreadName,void * pArguments,UINT32 interval)
{
	VOS_ERR_T		vos_err = VOS_NO_ERR;
	VOS_THREAD_T	ThreadHandle;
	vos_err = vos_threadCreate(
					&ThreadHandle,					/* Pointer to returned thread handle */
					ThreadName,						/* Pointer to name of the thread (optional) */
					VOS_THREAD_POLICY_OTHER,					/* Scheduling policy (FIFO, Round Robin or other) */
					100,											/* Scheduling priority (1...255 (highest), default 0) */
					interval,											/* Interval for cyclic threads in us (optional) */
					threadStackSize,							/* Minimum stacksize, default 0: 16kB */
					pfunction,			/* Pointer to the thread function */
					pArguments);	/* Pointer to the thread function parameters */
	if (vos_err == VOS_NO_ERR)
	{
		vos_printLog(VOS_LOG_DBG, "DDS Thread Create success,ThreadName:%s\n",ThreadName);
		return 1;
	}
	else
	{
		vos_printLog(VOS_LOG_ERROR, "DDS Thread Create Err,ThreadName:%s\n",ThreadName);
		return 0;
	}
	
}

void ddsReadSendRedunData()
{
	static UINT32 redunReqCounter=0;
	int ddsRC=0;
	//DDS向冗余发送请求
	UINT8 dataReqRedun[APP_TO_REDUN_DDS_DATALENTH]={0};
	UINT32 lenth_req=APP_TO_REDUN_DDS_DATALENTH;
	//请求数据包内容
	dataReqRedun[1]=21;//数据长度
	dataReqRedun[2]=2;//帧格式
	UINT32	tempcounter = htonl(redunReqCounter);//序列号
	memcpy(dataReqRedun+3, (void *)&tempcounter, 4);
	
			
	//ddsRC=SendDDSData(APP_REDUN_CM_ID, dataReqRedun,lenth_req);
	if (1==ddsRC)
	{
		redunReqCounter++;
	}

	else
		vos_printLog(VOS_LOG_ERROR, "DDS send redundancy request faild\n");
	
	//DDS接收冗余消息
	ddsRC=0;
	UINT32	redunDataLenth=0;
	UINT8 dataRedunMsg[DDS_DATALENTH_MAX]={0};

	//ddsRC=RecvDDSData(APP_REDUN_CM_ID,dataRedunMsg,&redunDataLenth,1,1);
	
	if (1==ddsRC)
	{
		UINT32 redunRC=parse_DDSredundant_message(dataRedunMsg,redunDataLenth);
		if(redunRC==1 || redunRC==0)
		{
			SetRedundant(redunRC);
		}	
	}
	else
	vos_printLog(VOS_LOG_ERROR, "DDS recv redundancy message faild\n");
}

UINT32 parse_DDSredundant_message(UINT8* data,UINT32 datalenth)
{
	UINT8 buffer[DDS_DATALENTH_MAX]={0};
	memcpy((void *)buffer, (const void *)data, datalenth);
	static UINT8 last_redunState=3;
	UINT32 stopTrdpSendOrNot=0;


    //判断buffer的内容
	if(last_redunState==buffer[3])
	{
		vos_printLog(VOS_LOG_ERROR, "redundant status stay %d\n",last_redunState);
		return 2;
	}
	else
	{
		last_redunState=buffer[3];
		if (3==buffer[3])
		{
			printf("new redundant status ,code:%d\n",buffer[3]);
			stopTrdpSendOrNot=1;
		}
		else if(4<buffer[3])
		{
			stopTrdpSendOrNot=2;
			printf("invalid status code:%d\n",buffer[3]);
		}
		else
		{
			stopTrdpSendOrNot=0;
			printf("new redundant status code:%d\n",buffer[3]);
		}
	}	

	return stopTrdpSendOrNot;
}
void SetRedundant(UINT32 stopTrdpSendOrNot)
{
	TRDP_ERR_T rc=TRDP_NO_ERR;
	BOOL8 ifleader=0;
	
	ifleader=stopTrdpSendOrNot==1? 1:0;
	
	rc=tlp_setRedundant(appHandleList[0], 1, ifleader);
	rc=tlp_setRedundant(appHandleList[1], 1, ifleader);
	
	if (rc==TRDP_NO_ERR)
		printf("change redundant state success,work in %s mode,code:%d\n",ifleader==1 ? "MASTER" : "SLAVE",ifleader);

	else
		printf("change redundant state failed,supposed to work in %s mode,code:%d\n",ifleader==1 ? "MASTER" : "SLAVE",ifleader);
}

#endif /* TRDP_OPTION_LADDER */



